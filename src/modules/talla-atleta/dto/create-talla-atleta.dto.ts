import { IsBoolean, IsString, IsUUID } from 'class-validator';
import { Atleta } from 'src/modules/atletas/entities/atleta.entity';

export class CreateTallaAtletaDto {
    _id?: string;

    @IsString()
    tallaFranela: string;

    @IsString()
    tallaPantalon: string;

    @IsString()
    tallaZapatos: string;

    @IsBoolean()
    statusTalla: boolean;

    @IsUUID()
    atleta: Atleta;
}
