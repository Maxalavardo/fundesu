import { IsString, IsUUID } from 'class-validator';
import { ComplejosDeportivo } from 'src/modules/complejos-deportivos/entities/complejos-deportivo.entity';

export class CreateImgComplejoDepDto {
    _id?: string;

    @IsString()
    image?: string;

    @IsUUID()
    complejo: ComplejosDeportivo;
}
