import { Estado } from 'src/modules/estados/entities/estado.entity';
import { Parroquia } from 'src/modules/parroquias/entities/parroquia.entity';
import { Usuario } from 'src/modules/usuarios/entities/usuario.entity';
import { Column, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Municipio {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreMunicipio: string;

    @Column({ type: Number, nullable: true })
    index: number | null;

    @ManyToOne(() => Estado, (estado) => estado.municipio, { eager: true })
    estado: Estado;

    @Column({ nullable: true })
    @DeleteDateColumn()
    deleteAt?: Date;

    @OneToMany(() => Parroquia, (parroquia) => parroquia.municipio)
    parroquia: Parroquia;

    @OneToMany(() => Usuario, (usuario) => usuario.municipio)
    usuario: Usuario;
}
