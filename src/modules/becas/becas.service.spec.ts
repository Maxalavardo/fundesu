import { Test, TestingModule } from '@nestjs/testing';
import { BecasService } from './becas.service';

describe('BecasService', () => {
    let service: BecasService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [BecasService],
        }).compile();

        service = module.get<BecasService>(BecasService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
