import { ImgSuperficie } from 'src/modules/img-superficie/entities/img-superficie.entity';
import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { MaterialSuperficie } from 'src/modules/material-superficie/entities/material-superficie.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class SuperficieInstalacion {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreSuperficie: string;

    @Column({ type: 'float' })
    medidaSuperficie: number;

    @ManyToOne(() => InstalacionesDeportiva, (instalaciones) => instalaciones.superficie, { eager: true })
    instalaciones: InstalacionesDeportiva;

    @OneToMany(() => MaterialSuperficie, (superficieMaterial) => superficieMaterial.superficie)
    superficieMaterial: MaterialSuperficie;
}
