import { Test, TestingModule } from '@nestjs/testing';
import { ImgComplementoController } from './img-complemento.controller';
import { ImgComplementoService } from './img-complemento.service';

describe('ImgComplementoController', () => {
    let controller: ImgComplementoController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImgComplementoController],
            providers: [ImgComplementoService],
        }).compile();

        controller = module.get<ImgComplementoController>(ImgComplementoController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
