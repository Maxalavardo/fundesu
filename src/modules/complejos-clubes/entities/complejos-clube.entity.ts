import { Clube } from 'src/modules/clubes/entities/clube.entity';
import { ComplejosDeportivo } from 'src/modules/complejos-deportivos/entities/complejos-deportivo.entity';
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ComplejosClube {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @ManyToOne(() => ComplejosDeportivo, (complejo) => complejo.complejoClub, { eager: true })
    complejo: ComplejosDeportivo;

    @ManyToOne(() => Clube, (club) => club.complejoClub, { eager: true })
    club: Clube;
}
