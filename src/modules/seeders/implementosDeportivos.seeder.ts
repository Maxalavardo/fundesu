import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { ImplementosDeportivo } from '../implementos-deportivos/entities/implementos-deportivo.entity';

@Injectable()
export class ImplementosDeportivosSeeder implements Seeder {
    constructor(
        @InjectRepository(ImplementosDeportivo)
        private readonly implementosDeportivosRepository: Repository<ImplementosDeportivo>,
    ) {}

    async seed(): Promise<any> {
        const { implementosDeportivos } = JSON.parse(readFileSync('./src/modules/seeders/jsons/implementosDeportivos.json', 'utf8'));

        try {
            for await (const implementoDeportivo of implementosDeportivos) {
                await this.implementosDeportivosRepository.save({ nombreImplemento: implementoDeportivo });
            }
        } catch (exception: any) {
            console.log('Crear implemento deportivo: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const implementosDeportivo = await this.implementosDeportivosRepository.find();
        return await this.implementosDeportivosRepository.remove(implementosDeportivo);
    }
}
