import { Usuario } from '../entities/usuario.entity';

export class UserTransformer {
    public static transform(user: Usuario) {
        return {
            id: user._id,
            email: user.usuario,
            role: user.rol,
            created_at: user.createdAt,
            updated_at: user.updatedAt,
            person: user.persona,
        };
    }
}
