import { PartialType } from '@nestjs/mapped-types';
import { CreateTechadoInstalacionDto } from './create-techado-instalacion.dto';

export class UpdateTechadoInstalacionDto extends PartialType(CreateTechadoInstalacionDto) {}
