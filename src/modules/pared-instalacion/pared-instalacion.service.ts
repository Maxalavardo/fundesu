import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateParedInstalacionDto } from './dto/create-pared-instalacion.dto';
import { UpdateParedInstalacionDto } from './dto/update-pared-instalacion.dto';
import { ParedInstalacion } from './entities/pared-instalacion.entity';

@Injectable()
export class ParedInstalacionService {
    constructor(
        @InjectRepository(ParedInstalacion)
        private readonly paredInstaRepository: Repository<ParedInstalacion>,
    ) {}

    async create(request: CreateParedInstalacionDto) {
        const pared = await this.paredInstaRepository.create(request);
        return await this.paredInstaRepository.save(pared);
    }

    async findAll() {
        return await this.paredInstaRepository.find();
    }

    async findOne(id: string) {
        const complemento = await this.paredInstaRepository
            .createQueryBuilder('i')
            .where('instalaciones_id = :id', { id });
        return await complemento.getMany();
    }

    async update(id: string, request: UpdateParedInstalacionDto) {
        const upPared = await this.paredInstaRepository.findOne({ where: { _id: id } });
        upPared.instalaciones = request.instalaciones;
        await this.paredInstaRepository.merge(upPared, request);
        return await this.paredInstaRepository.save(upPared);
    }

    async remove(id: string) {
        return await this.paredInstaRepository.delete(id);
    }
}
