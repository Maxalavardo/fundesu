import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateAtletaDto } from './dto/create-atleta.dto';
import { UpdateAtletaDto } from './dto/update-atleta.dto';
import { Atleta } from './entities/atleta.entity';
import { ViewAtleta } from './entities/viewAtleta.entity';

@Injectable()
export class AtletasService {
    constructor(
        @InjectRepository(Atleta)
        private readonly atletaRepository: Repository<Atleta>,
        @InjectRepository(ViewAtleta)
        private readonly viewRepository: Repository<ViewAtleta>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    async create(request: CreateAtletaDto, user: any) {
        let atleta = this.atletaRepository.create(request);
        atleta = await this.atletaRepository.save(atleta);

        const message = 'Un atelta a sido registrado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Atleta.name,
            uuid: atleta._id,
            data: atleta,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return atleta;
    }

    async findAll() {
        return await this.atletaRepository.find();
    }

    async findOne(id: string) {
        return await this.atletaRepository.findOne({ where: { _id: id } });
    }

    async count() {
        const count = await this.atletaRepository.count();
        return count;
    }

    async view(id: string) {
        return await this.viewRepository.find({ where: { _id: id } });
    }

    async update(id: string, request: UpdateAtletaDto) {
        const upAtleta = await this.atletaRepository.findOne({ where: { _id: id } });
        this.atletaRepository.merge(upAtleta, request);
        return await this.atletaRepository.save(upAtleta);
    }

    async remove(id: string, user: any) {
        const atleta = await this.atletaRepository.findOne({ where: { _id: id } });

        const message = 'Un atelta a sido eliminado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Atleta.name,
            uuid: id,
            data: atleta,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.atletaRepository.softDelete(id);
    }
}
