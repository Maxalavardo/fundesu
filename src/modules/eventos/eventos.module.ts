import { Module } from '@nestjs/common';
import { EventosService } from './eventos.service';
import { EventosController } from './eventos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Evento } from './entities/evento.entity';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Evento, Notification, Usuario])],
    controllers: [EventosController],
    providers: [EventosService],
})
export class EventosModule {}
