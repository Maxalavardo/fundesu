import { IsNumber, IsString, IsUUID } from 'class-validator';
import { InstalacionesDeportiva } from '../../instalaciones-deportivas/entities/instalaciones-deportiva.entity';

export class CreateSuperficieInstalacionDto {
    _id?: string;

    @IsString()
    nombreSuperficie: string;

    @IsNumber()
    medidaSuperficie: number;

    @IsUUID()
    instalaciones: InstalacionesDeportiva;
}
