import { Controller, Get, Post, UseGuards, UseInterceptors, Request, Res } from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { AuthAll } from './decorator/auth.decorator';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { LoginInterceptor } from './transformers/auth.interceptor';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @UseGuards(LocalAuthGuard)
    @UseInterceptors(LoginInterceptor)
    @Post('login')
    async login(@Request() req) {
        return await this.authService.login(req.user);
    }

    @AuthAll()
    @Post('logout')
    @ApiOperation({ summary: 'Logout' })
    singOut(@Res() res) {
        res.send({ statusCode: res.statusCode, data: { token: '' } });
    }

    @AuthAll()
    @Get('me')
    async profile(@Request() req) {
        return await this.authService.profile(req.user);
    }
}
