import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateCargoDto } from './dto/create-cargo.dto';
import { UpdateCargoDto } from './dto/update-cargo.dto';
import { Cargo } from './entities/cargo.entity';

@Injectable()
export class CargosService {
    constructor(
        @InjectRepository(Cargo)
        private readonly cargoRepository: Repository<Cargo>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    async create(request: CreateCargoDto, user: any) {
        let cargo = this.cargoRepository.create(request);
        cargo = await this.cargoRepository.save(cargo);

        const message = 'Un cargo a sido registrado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Cargo.name,
            uuid: cargo._id,
            data: cargo,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return cargo;
    }

    async findAll() {
        return await this.cargoRepository.find();
    }

    async findOne(id: string) {
        return await this.cargoRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateCargoDto) {
        const upCargo = await this.cargoRepository.findOne({ where: { _id: id } });
        this.cargoRepository.merge(upCargo, request);
        return await this.cargoRepository.save(upCargo);
    }

    async remove(id: string, user: any) {
        const cargo = await this.cargoRepository.findOne({ where: { _id: id } });

        const message = 'Un cargo a sido eliminado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Cargo.name,
            uuid: id,
            data: cargo,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.cargoRepository.softDelete(id);
    }
}
