import { Module } from '@nestjs/common';
import { ImgTechadoService } from './img-techado.service';
import { ImgTechadoController } from './img-techado.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImgTechado } from './entities/img-techado.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImgTechado])],
    controllers: [ImgTechadoController],
    providers: [ImgTechadoService],
})
export class ImgTechadoModule {}
