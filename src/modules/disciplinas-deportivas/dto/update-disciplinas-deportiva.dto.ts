import { PartialType } from '@nestjs/mapped-types';
import { CreateDisciplinasDeportivaDto } from './create-disciplinas-deportiva.dto';

export class UpdateDisciplinasDeportivaDto extends PartialType(CreateDisciplinasDeportivaDto) {}
