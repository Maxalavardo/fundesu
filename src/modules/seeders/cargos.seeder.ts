import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { Cargo } from '../cargos/entities/cargo.entity';

@Injectable()
export class CargosSeeder implements Seeder {
    constructor(
        @InjectRepository(Cargo)
        private readonly cargosRepository: Repository<Cargo>,
    ) {}

    async seed(): Promise<any> {
        const { cargos } = JSON.parse(readFileSync('./src/modules/seeders/jsons/cargos.json', 'utf8'));

        try {
            for await (const cargo of cargos) {
                await this.cargosRepository.save({ nombreCargo: cargo });
            }
        } catch (exception: any) {
            console.log('Crear tipo instalacion deportiva: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const cargo = await this.cargosRepository.find();
        return await this.cargosRepository.remove(cargo);
    }
}
