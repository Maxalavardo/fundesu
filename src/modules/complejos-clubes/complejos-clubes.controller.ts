import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { ComplejosClubesService } from './complejos-clubes.service';
import { CreateComplejosClubeDto } from './dto/create-complejos-clube.dto';
import { UpdateComplejosClubeDto } from './dto/update-complejos-clube.dto';

@Controller('complejos-clubes')
@ApiTags('Complejos-clubes')
export class ComplejosClubesController {
    constructor(private readonly complejosClubesService: ComplejosClubesService) {}

    @Post()
    @ApiBody({ type: [CreateComplejosClubeDto] })
    create(@Res() Res, @Body() createComplejosClubeDto: CreateComplejosClubeDto) {
        return this.complejosClubesService
            .create(createComplejosClubeDto)
            .then((club) => {
                return Res.status(HttpStatus.CREATED).json(club);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.complejosClubesService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.complejosClubesService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateComplejosClubeDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateComplejosClubeDto: UpdateComplejosClubeDto) {
        return this.complejosClubesService
            .update(id, updateComplejosClubeDto)
            .then((club) => {
                return Res.status(HttpStatus.ACCEPTED).json(club);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.complejosClubesService.remove(id);
    }
}
