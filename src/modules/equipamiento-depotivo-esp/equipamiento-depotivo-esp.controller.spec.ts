import { Test, TestingModule } from '@nestjs/testing';
import { EquipamientoDepotivoEspController } from './equipamiento-depotivo-esp.controller';
import { EquipamientoDepotivoEspService } from './equipamiento-depotivo-esp.service';

describe('EquipamientoDepotivoEspController', () => {
    let controller: EquipamientoDepotivoEspController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [EquipamientoDepotivoEspController],
            providers: [EquipamientoDepotivoEspService],
        }).compile();

        controller = module.get<EquipamientoDepotivoEspController>(EquipamientoDepotivoEspController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
