import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEquipamientoDepotivoEspDto } from './dto/create-equipamiento-depotivo-esp.dto';
import { UpdateEquipamientoDepotivoEspDto } from './dto/update-equipamiento-depotivo-esp.dto';
import { EquipamientoDepotivoEsp } from './entities/equipamiento-depotivo-esp.entity';

@Injectable()
export class EquipamientoDepotivoEspService {
    constructor(
        @InjectRepository(EquipamientoDepotivoEsp)
        private readonly equipamientoRepositoy: Repository<EquipamientoDepotivoEsp>,
    ) {}

    async create(request: CreateEquipamientoDepotivoEspDto): Promise<EquipamientoDepotivoEsp> {
        const equipamiento = await this.equipamientoRepositoy.create(request);
        return await this.equipamientoRepositoy.save(equipamiento);
    }

    async findAll() {
        return await this.equipamientoRepositoy.find();
    }

    async findOne(id: string) {
        return await this.equipamientoRepositoy.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateEquipamientoDepotivoEspDto): Promise<EquipamientoDepotivoEsp> {
        const upEquipamiento = await this.equipamientoRepositoy.findOne({ where: { _id: id } });
        await this.equipamientoRepositoy.merge(upEquipamiento, request);
        return await this.equipamientoRepositoy.save(upEquipamiento);
    }

    async remove(id: string) {
        return await this.equipamientoRepositoy.softDelete(id);
    }
}
