import { Test, TestingModule } from '@nestjs/testing';
import { ComplejosClubesService } from './complejos-clubes.service';

describe('ComplejosClubesService', () => {
    let service: ComplejosClubesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ComplejosClubesService],
        }).compile();

        service = module.get<ComplejosClubesService>(ComplejosClubesService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
