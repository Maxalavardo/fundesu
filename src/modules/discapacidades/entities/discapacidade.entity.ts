import { DiscapacidadesAtleta } from 'src/modules/discapacidades-atleta/entities/discapacidades-atleta.entity';
import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Discapacidade {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreDiscapacidad: string;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => DiscapacidadesAtleta, (discapacidadAtleta) => discapacidadAtleta.discapacidad)
    discapacidadAtleta: DiscapacidadesAtleta;
}
