import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateDisciplinasDeportivaDto } from './dto/create-disciplinas-deportiva.dto';
import { UpdateDisciplinasDeportivaDto } from './dto/update-disciplinas-deportiva.dto';
import { DisciplinasDeportiva } from './entities/disciplinas-deportiva.entity';

@Injectable()
export class DisciplinasDeportivasService {
    constructor(
        @InjectRepository(DisciplinasDeportiva)
        private readonly disciplinasDeportRepository: Repository<DisciplinasDeportiva>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    async create(request: CreateDisciplinasDeportivaDto, user: any): Promise<DisciplinasDeportiva> {
        let disciplinaD = this.disciplinasDeportRepository.create(request);
        disciplinaD = await this.disciplinasDeportRepository.save(disciplinaD);

        const message = 'Una disciplina deportiva a sido registrada';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: DisciplinasDeportiva.name,
            uuid: disciplinaD._id,
            data: disciplinaD,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return disciplinaD;
    }

    async findAll() {
        return await this.disciplinasDeportRepository.find();
    }

    async findOne(id: string) {
        return await this.disciplinasDeportRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateDisciplinasDeportivaDto): Promise<DisciplinasDeportiva> {
        const upDisciplinaD = await this.disciplinasDeportRepository.findOne({ where: { _id: id } });
        this.disciplinasDeportRepository.merge(upDisciplinaD, request);
        return await this.disciplinasDeportRepository.save(upDisciplinaD);
    }

    async remove(id: string, user: any) {
        const disciplinaD = await this.disciplinasDeportRepository.findOne({ where: { _id: id } });

        const message = 'Una disciplina deportiva a sido eliminada';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: DisciplinasDeportiva.name,
            uuid: id,
            data: disciplinaD,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.disciplinasDeportRepository.softDelete(id);
    }
}
