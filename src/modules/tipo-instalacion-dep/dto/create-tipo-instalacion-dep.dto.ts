import { IsString } from 'class-validator';

export class CreateTipoInstalacionDepDto {
    _id?: string;

    @IsString()
    nombreTipoInstalacion: string;
}
