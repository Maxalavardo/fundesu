import { Materiale } from '../../materiales/entities/materiale.entity';
import { IluminacionInstalacion } from '../../iluminacion-instalacion/entities/iluminacion-instalacion.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';

@Entity()
export class MaterialIluminacion {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'integer' })
    cantidadMaterial: number;

    @ManyToOne(() => Materiale, (material) => material.iluminacionMaterial, { eager: true })
    material: Materiale;

    @ManyToOne(() => IluminacionInstalacion, (iluminacion) => iluminacion.iluminacionMaterial, { eager: true })
    iluminacion: IluminacionInstalacion;
}
