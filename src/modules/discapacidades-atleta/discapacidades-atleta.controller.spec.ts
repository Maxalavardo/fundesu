import { Test, TestingModule } from '@nestjs/testing';
import { DiscapacidadesAtletaController } from './discapacidades-atleta.controller';
import { DiscapacidadesAtletaService } from './discapacidades-atleta.service';

describe('DiscapacidadesAtletaController', () => {
    let controller: DiscapacidadesAtletaController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [DiscapacidadesAtletaController],
            providers: [DiscapacidadesAtletaService],
        }).compile();

        controller = module.get<DiscapacidadesAtletaController>(DiscapacidadesAtletaController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
