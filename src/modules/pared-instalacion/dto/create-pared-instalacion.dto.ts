import { IsNumber, IsString, IsUUID } from 'class-validator';
import { InstalacionesDeportiva } from '../../instalaciones-deportivas/entities/instalaciones-deportiva.entity';

export class CreateParedInstalacionDto {
    _id?: string;

    @IsString()
    nombrePared: string;

    @IsNumber()
    medidaPared: number;

    @IsUUID()
    instalaciones: InstalacionesDeportiva;
}
