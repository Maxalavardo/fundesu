import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Materiale } from '../materiales/entities/materiale.entity';
import { CreateMaterialComplementoDto } from './dto/create-material-complemento.dto';
import { UpdateMaterialComplementoDto } from './dto/update-material-complemento.dto';
import { MaterialComplemento } from './entities/material-complemento.entity';

@Injectable()
export class MaterialComplementoService {
    constructor(
        @InjectRepository(MaterialComplemento)
        private readonly materialComRepository: Repository<MaterialComplemento>,
        @InjectRepository(Materiale)
        private readonly materialRespository: Repository<Materiale>,
    ) {}

    async create(request: CreateMaterialComplementoDto) {
        const complemento = await this.materialComRepository.create(request);
        return await this.materialComRepository.save(complemento);
    }

    async findAll() {
        return await this.materialComRepository.find();
    }

    async findOne(id: string) {
        const material = await this.materialComRepository.find({
            relations: [],
            where: {
                complemento: {
                    _id: id,
                },
            },
        });
        return material;
    }

    async update(id: string, request: UpdateMaterialComplementoDto) {
        const upComplemento = await this.materialComRepository.findOne({ where: { _id: id } });
        upComplemento.complemento = request.complemento;
        upComplemento.material = request.material;
        await this.materialComRepository.merge(upComplemento, request);
        return await this.materialComRepository.save(upComplemento);
    }

    async remove(id: string) {
        return await this.materialComRepository.delete(id);
    }
}
