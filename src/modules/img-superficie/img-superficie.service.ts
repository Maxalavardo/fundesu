import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImgSuperficieDto } from './dto/create-img-superficie.dto';
import { UpdateImgSuperficieDto } from './dto/update-img-superficie.dto';
import { ImgSuperficie } from './entities/img-superficie.entity';

@Injectable()
export class ImgSuperficieService {
    constructor(
        @InjectRepository(ImgSuperficie)
        private readonly imgSuperficieRepository: Repository<ImgSuperficie>,
    ) {}

    async create(request: CreateImgSuperficieDto) {
        const img = await this.imgSuperficieRepository.create(request);
        return await this.imgSuperficieRepository.save(img);
    }

    async findAll() {
        return await this.imgSuperficieRepository.find();
    }

    async findOne(id: string) {
        return await this.imgSuperficieRepository.findOne({
            where: {
                instalaciones: { _id: id },
            },
        });
    }

    async update(id: string, request: UpdateImgSuperficieDto) {
        const upImg = await this.imgSuperficieRepository.findOne({ where: { _id: id } });
        upImg.instalaciones = request.instalaciones;
        await this.imgSuperficieRepository.merge(upImg, request);
        return await this.imgSuperficieRepository.save(upImg);
    }

    async remove(id: string) {
        return await this.imgSuperficieRepository.delete(id);
    }
}
