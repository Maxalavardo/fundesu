import { Module } from '@nestjs/common';
import { ImgInstalacionesService } from './img-instalaciones.service';
import { ImgInstalacionesController } from './img-instalaciones.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImgInstalacione } from './entities/img-instalacione.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImgInstalacione])],
    controllers: [ImgInstalacionesController],
    providers: [ImgInstalacionesService],
})
export class ImgInstalacionesModule {}
