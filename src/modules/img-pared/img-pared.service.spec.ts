import { Test, TestingModule } from '@nestjs/testing';
import { ImgParedService } from './img-pared.service';

describe('ImgParedService', () => {
    let service: ImgParedService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImgParedService],
        }).compile();

        service = module.get<ImgParedService>(ImgParedService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
