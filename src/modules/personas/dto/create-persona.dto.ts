import { IsDateString, IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { Sector } from 'src/modules/sectores/entities/sectore.entity';

export class CreatePersonaDto {
    _id?: string;

    @IsNotEmpty()
    @IsString()
    cedula: string;

    @IsNotEmpty()
    @IsString()
    nombres: string;

    @IsNotEmpty()
    @IsString()
    apellidos: string;

    @IsNotEmpty()
    @IsString()
    telefono: string;

    @IsNotEmpty()
    @IsString()
    tlfcasa: string;

    @IsNotEmpty()
    @IsString()
    sexo: string;

    @IsDateString()
    fechanac: Date;

    @IsNotEmpty()
    @IsString()
    direccion: string;

    @IsUUID()
    sector: Sector;
}
