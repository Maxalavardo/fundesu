import { PartialType } from '@nestjs/mapped-types';
import { CreateImgTechadoDto } from './create-img-techado.dto';

export class UpdateImgTechadoDto extends PartialType(CreateImgTechadoDto) {}
