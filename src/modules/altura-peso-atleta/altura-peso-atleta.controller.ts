import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Res } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AlturaPesoAtletaService } from './altura-peso-atleta.service';
import { CreateAlturaPesoAtletaDto } from './dto/create-altura-peso-atleta.dto';
import { UpdateAlturaPesoAtletaDto } from './dto/update-altura-peso-atleta.dto';

@ApiTags('Altura-Peso-Atleta')
@Controller('altura-peso-atleta')
export class AlturaPesoAtletaController {
    constructor(private readonly alturaPesoAtletaService: AlturaPesoAtletaService) {}

    @Post()
    @ApiBody({ type: [CreateAlturaPesoAtletaDto] })
    create(@Res() Res, @Body() createAlturaPesoAtletaDto: CreateAlturaPesoAtletaDto) {
        this.alturaPesoAtletaService
            .create(createAlturaPesoAtletaDto)
            .then((atleta) => {
                return Res.status(HttpStatus.CREATED).json(atleta);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.alturaPesoAtletaService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.alturaPesoAtletaService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateAlturaPesoAtletaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateAlturaPesoAtletaDto: UpdateAlturaPesoAtletaDto) {
        this.alturaPesoAtletaService
            .update(id, updateAlturaPesoAtletaDto)
            .then((atleta) => {
                return Res.status(HttpStatus.ACCEPTED).json(atleta);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.alturaPesoAtletaService.remove(id);
    }
}
