import { Test, TestingModule } from '@nestjs/testing';
import { MaterialComplementoController } from './material-complemento.controller';
import { MaterialComplementoService } from './material-complemento.service';

describe('MaterialComplementoController', () => {
    let controller: MaterialComplementoController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [MaterialComplementoController],
            providers: [MaterialComplementoService],
        }).compile();

        controller = module.get<MaterialComplementoController>(MaterialComplementoController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
