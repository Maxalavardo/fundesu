import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ImgSuperficie {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'text', nullable: true })
    image: string;

    @ManyToOne(() => InstalacionesDeportiva, (instalaciones) => instalaciones.imageSuperficie, {
        eager: true,
    })
    instalaciones: InstalacionesDeportiva;
}
