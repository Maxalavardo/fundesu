import { Test, TestingModule } from '@nestjs/testing';
import { ImgComplementoService } from './img-complemento.service';

describe('ImgComplementoService', () => {
    let service: ImgComplementoService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImgComplementoService],
        }).compile();

        service = module.get<ImgComplementoService>(ImgComplementoService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
