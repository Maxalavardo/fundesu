import { Usuario } from 'src/modules/usuarios/entities/usuario.entity';
import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity('notification')
export class Notification {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    notification: string;

    @Column({ type: 'varchar' })
    entity: string;

    @Column({ type: 'uuid' })
    uuid: string;

    @Column({ type: 'simple-json' })
    data: any;

    @Column({ type: 'boolean', default: false })
    read?: boolean;

    @CreateDateColumn({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP(6)' })
    createdAt: Date;

    @UpdateDateColumn({
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP(6)',
        onUpdate: 'CURRENT_TIMESTAMP(6)',
    })
    updatedAt: Date;

    @ManyToOne(() => Usuario, (user) => user.notification, { eager: true })
    user: Usuario;
}
