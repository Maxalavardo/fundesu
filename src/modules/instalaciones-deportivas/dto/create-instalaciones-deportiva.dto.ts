import { IsBoolean, IsNumber, IsString, IsUUID } from 'class-validator';
import { ComplejosDeportivo } from 'src/modules/complejos-deportivos/entities/complejos-deportivo.entity';
import { TipoInstalacionDep } from 'src/modules/tipo-instalacion-dep/entities/tipo-instalacion-dep.entity';

export class CreateInstalacionesDeportivaDto {
    _id?: string;

    @IsString()
    nombreInstalacion: string;

    @IsNumber()
    medidaLargo: number;

    @IsNumber()
    medidaAncho: number;

    @IsNumber()
    metrosCuadrados: number;

    @IsString()
    tipoEspacio: string;

    @IsString()
    estadoInstalacion: string;

    @IsBoolean()
    recuperacionInstalacion: boolean;

    @IsUUID()
    complejosDeportivos: ComplejosDeportivo;

    @IsUUID()
    tipoInstalacionDep: TipoInstalacionDep;
}
