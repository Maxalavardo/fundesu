import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateInstalacionDisciplinaDto } from './dto/create-instalacion-disciplina.dto';
import { UpdateInstalacionDisciplinaDto } from './dto/update-instalacion-disciplina.dto';
import { InstalacionDisciplina } from './entities/instalacion-disciplina.entity';

@Injectable()
export class InstalacionDisciplinaService {
    constructor(
        @InjectRepository(InstalacionDisciplina)
        private readonly instalacionDisciplinaRepository: Repository<InstalacionDisciplina>,
    ) {}

    async create(request: CreateInstalacionDisciplinaDto) {
        let instalacionDisc = this.instalacionDisciplinaRepository.create(request);
        instalacionDisc = await this.instalacionDisciplinaRepository.save(instalacionDisc);

        return instalacionDisc;
    }

    async findAll() {
        return await this.instalacionDisciplinaRepository.find();
    }

    async findOne(id: string) {
        return await this.instalacionDisciplinaRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateInstalacionDisciplinaDto) {
        const upInstalacionDisc = await this.instalacionDisciplinaRepository.findOne({ where: { _id: id } });
        upInstalacionDisc.disciplina = request.disciplina;
        upInstalacionDisc.tipoInstalacion = request.tipoInstalacion;
        await this.instalacionDisciplinaRepository.merge(upInstalacionDisc, request);
        return await this.instalacionDisciplinaRepository.save(upInstalacionDisc);
    }

    async remove(id: string) {
        return await this.instalacionDisciplinaRepository.delete(id);
    }
}
