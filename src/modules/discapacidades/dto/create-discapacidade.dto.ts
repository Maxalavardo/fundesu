import { IsString } from 'class-validator';

export class CreateDiscapacidadeDto {
    _id?: string;

    @IsString()
    nombreDiscapacidad: string;
}
