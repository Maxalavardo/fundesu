import { Test, TestingModule } from '@nestjs/testing';
import { TechadoInstalacionController } from './techado-instalacion.controller';
import { TechadoInstalacionService } from './techado-instalacion.service';

describe('TechadoInstalacionController', () => {
    let controller: TechadoInstalacionController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [TechadoInstalacionController],
            providers: [TechadoInstalacionService],
        }).compile();

        controller = module.get<TechadoInstalacionController>(TechadoInstalacionController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
