import { Test, TestingModule } from '@nestjs/testing';
import { ImplementosInstalacionController } from './implementos-instalacion.controller';
import { ImplementosInstalacionService } from './implementos-instalacion.service';

describe('ImplementosInstalacionController', () => {
    let controller: ImplementosInstalacionController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImplementosInstalacionController],
            providers: [ImplementosInstalacionService],
        }).compile();

        controller = module.get<ImplementosInstalacionController>(ImplementosInstalacionController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
