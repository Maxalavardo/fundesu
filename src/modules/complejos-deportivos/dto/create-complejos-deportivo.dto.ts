import { IsString, IsUUID } from 'class-validator';
import { Sector } from 'src/modules/sectores/entities/sectore.entity';

export class CreateComplejosDeportivoDto {
    _id?: string;

    @IsString()
    nombreComplejoDeportivo: string;

    @IsString()
    direccion: string;

    @IsUUID()
    sector: Sector;
}
