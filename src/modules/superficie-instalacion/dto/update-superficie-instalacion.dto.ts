import { PartialType } from '@nestjs/mapped-types';
import { CreateSuperficieInstalacionDto } from './create-superficie-instalacion.dto';

export class UpdateSuperficieInstalacionDto extends PartialType(CreateSuperficieInstalacionDto) {}
