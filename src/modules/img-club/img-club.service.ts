import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Clube } from '../clubes/entities/clube.entity';
import { CreateImgClubDto } from './dto/create-img-club.dto';
import { UpdateImgClubDto } from './dto/update-img-club.dto';
import { ImgClub } from './entities/img-club.entity';

@Injectable()
export class ImgClubService {
    constructor(
        @InjectRepository(ImgClub)
        private readonly imgClubRepository: Repository<ImgClub>,
        @InjectRepository(Clube)
        private readonly clubRepository: Repository<Clube>,
    ) {}

    async create(request: CreateImgClubDto) {
        const img = await this.imgClubRepository.create(request);
        return await this.imgClubRepository.save(img);
    }

    async findAll() {
        return await this.imgClubRepository.find();
    }

    async findOne(id: string) {
        return await this.imgClubRepository.findOne({
            where: {
                club: {
                    _id: id,
                },
            },
        });
    }

    async update(id: string, request: UpdateImgClubDto) {
        const upImg = await this.imgClubRepository.findOne({ where: { _id: id } });
        upImg.club = request.club;
        await this.imgClubRepository.merge(upImg, request);
        return await this.imgClubRepository.save(upImg);
    }

    async remove(id: string) {
        return await this.imgClubRepository.delete(id);
    }
}
