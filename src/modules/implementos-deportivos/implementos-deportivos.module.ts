import { Module } from '@nestjs/common';
import { ImplementosDeportivosService } from './implementos-deportivos.service';
import { ImplementosDeportivosController } from './implementos-deportivos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImplementosDeportivo } from './entities/implementos-deportivo.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImplementosDeportivo])],
    controllers: [ImplementosDeportivosController],
    providers: [ImplementosDeportivosService],
})
export class ImplementosDeportivosModule {}
