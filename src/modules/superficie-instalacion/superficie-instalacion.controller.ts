import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Res } from '@nestjs/common';
import { SuperficieInstalacionService } from './superficie-instalacion.service';
import { CreateSuperficieInstalacionDto } from './dto/create-superficie-instalacion.dto';
import { UpdateSuperficieInstalacionDto } from './dto/update-superficie-instalacion.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('superficie-instalacion')
@ApiTags('Superficie-instalacion')
export class SuperficieInstalacionController {
    constructor(private readonly superficieInstalacionService: SuperficieInstalacionService) {}

    @Post()
    @ApiBody({ type: [CreateSuperficieInstalacionDto] })
    create(@Res() Res, @Body() createSuperficieInstalacionDto: CreateSuperficieInstalacionDto) {
        this.superficieInstalacionService
            .create(createSuperficieInstalacionDto)
            .then((superficie) => {
                return Res.status(HttpStatus.CREATED).json(superficie);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.superficieInstalacionService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.superficieInstalacionService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateSuperficieInstalacionDto] })
    update(
        @Res() Res,
        @Param('id') id: string,
        @Body() updateSuperficieInstalacionDto: UpdateSuperficieInstalacionDto,
    ) {
        this.superficieInstalacionService
            .update(id, updateSuperficieInstalacionDto)
            .then((superficie) => {
                return Res.status(HttpStatus.ACCEPTED).json(superficie);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.superficieInstalacionService.remove(id);
    }
}
