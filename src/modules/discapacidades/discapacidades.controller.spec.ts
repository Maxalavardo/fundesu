import { Test, TestingModule } from '@nestjs/testing';
import { DiscapacidadesController } from './discapacidades.controller';
import { DiscapacidadesService } from './discapacidades.service';

describe('DiscapacidadesController', () => {
    let controller: DiscapacidadesController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [DiscapacidadesController],
            providers: [DiscapacidadesService],
        }).compile();

        controller = module.get<DiscapacidadesController>(DiscapacidadesController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
