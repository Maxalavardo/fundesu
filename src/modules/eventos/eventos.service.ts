import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateEventoDto } from './dto/create-evento.dto';
import { UpdateEventoDto } from './dto/update-evento.dto';
import { Evento } from './entities/evento.entity';

@Injectable()
export class EventosService {
    constructor(
        @InjectRepository(Evento)
        private readonly eventoRepository: Repository<Evento>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    async create(request: CreateEventoDto, user: any) {
        let evento = this.eventoRepository.create(request);
        evento = await this.eventoRepository.save(evento);

        const message = 'Un evento a sido registrado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Evento.name,
            uuid: evento._id,
            data: evento,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return evento;
    }

    async findAll() {
        return await this.eventoRepository.find();
    }

    async findOne(id: string) {
        return await this.eventoRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateEventoDto) {
        const upEvento = await this.eventoRepository.findOne({ where: { _id: id } });
        this.eventoRepository.merge(upEvento, request);
        return await this.eventoRepository.save(upEvento);
    }

    async remove(id: string, user: any) {
        const evento = await this.eventoRepository.findOne({ where: { _id: id } });

        const message = 'Un evento a sido eliminado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Evento.name,
            uuid: id,
            data: evento,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.eventoRepository.softDelete(id);
    }
}
