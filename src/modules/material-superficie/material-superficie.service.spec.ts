import { Test, TestingModule } from '@nestjs/testing';
import { MaterialSuperficieService } from './material-superficie.service';

describe('MaterialSuperficieService', () => {
    let service: MaterialSuperficieService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MaterialSuperficieService],
        }).compile();

        service = module.get<MaterialSuperficieService>(MaterialSuperficieService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
