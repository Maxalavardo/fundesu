import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Res } from '@nestjs/common';
import { EventosAtletasService } from './eventos-atletas.service';
import { CreateEventosAtletaDto } from './dto/create-eventos-atleta.dto';
import { UpdateEventosAtletaDto } from './dto/update-eventos-atleta.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('eventos-atletas')
@ApiTags('Eventos-atletas')
export class EventosAtletasController {
    constructor(private readonly eventosAtletasService: EventosAtletasService) {}

    @Post()
    @ApiBody({ type: [CreateEventosAtletaDto] })
    create(@Res() Res, @Body() createEventosAtletaDto: CreateEventosAtletaDto) {
        return this.eventosAtletasService
            .create(createEventosAtletaDto)
            .then((evento) => {
                return Res.status(HttpStatus.CREATED).json(evento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.eventosAtletasService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.eventosAtletasService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateEventosAtletaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateEventosAtletaDto: UpdateEventosAtletaDto) {
        return this.eventosAtletasService
            .update(id, updateEventosAtletaDto)
            .then((evento) => {
                return Res.status(HttpStatus.ACCEPTED).json(evento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.eventosAtletasService.remove(id);
    }
}
