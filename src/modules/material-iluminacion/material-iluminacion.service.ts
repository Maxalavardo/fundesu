import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Materiale } from '../materiales/entities/materiale.entity';
import { CreateMaterialIluminacionDto } from './dto/create-material-iluminacion.dto';
import { UpdateMaterialIluminacionDto } from './dto/update-material-iluminacion.dto';
import { MaterialIluminacion } from './entities/material-iluminacion.entity';

@Injectable()
export class MaterialIluminacionService {
    constructor(
        @InjectRepository(MaterialIluminacion)
        private readonly materialIlumiRepository: Repository<MaterialIluminacion>,
        @InjectRepository(Materiale)
        private readonly materialRespository: Repository<Materiale>,
    ) {}

    async create(request: CreateMaterialIluminacionDto) {
        const iluminacion = await this.materialIlumiRepository.create(request);
        return await this.materialIlumiRepository.save(iluminacion);
    }

    async findAll() {
        return await this.materialIlumiRepository.find();
    }

    async findOne(id: string) {
        const material = await this.materialIlumiRepository.find({
            relations: [],
            where: {
                iluminacion: {
                    _id: id,
                },
            },
        });
        return material;
    }

    async update(id: string, request: UpdateMaterialIluminacionDto) {
        const upIluminacion = await this.materialIlumiRepository.findOne({ where: { _id: id } });
        upIluminacion.iluminacion = request.iluminacion;
        upIluminacion.material = request.material;
        await this.materialIlumiRepository.merge(upIluminacion, request);
        return await this.materialIlumiRepository.save(upIluminacion);
    }

    async remove(id: string) {
        return await this.materialIlumiRepository.delete(id);
    }
}
