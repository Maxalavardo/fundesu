import { Test, TestingModule } from '@nestjs/testing';
import { ParedInstalacionController } from './pared-instalacion.controller';
import { ParedInstalacionService } from './pared-instalacion.service';

describe('ParedInstalacionController', () => {
    let controller: ParedInstalacionController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ParedInstalacionController],
            providers: [ParedInstalacionService],
        }).compile();

        controller = module.get<ParedInstalacionController>(ParedInstalacionController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
