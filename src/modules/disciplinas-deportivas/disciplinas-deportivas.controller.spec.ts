import { Test, TestingModule } from '@nestjs/testing';
import { DisciplinasDeportivasController } from './disciplinas-deportivas.controller';
import { DisciplinasDeportivasService } from './disciplinas-deportivas.service';

describe('DisciplinasDeportivasController', () => {
    let controller: DisciplinasDeportivasController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [DisciplinasDeportivasController],
            providers: [DisciplinasDeportivasService],
        }).compile();

        controller = module.get<DisciplinasDeportivasController>(DisciplinasDeportivasController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
