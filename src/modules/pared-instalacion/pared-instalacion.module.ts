import { Module } from '@nestjs/common';
import { ParedInstalacionService } from './pared-instalacion.service';
import { ParedInstalacionController } from './pared-instalacion.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParedInstalacion } from './entities/pared-instalacion.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ParedInstalacion])],
    controllers: [ParedInstalacionController],
    providers: [ParedInstalacionService],
})
export class ParedInstalacionModule {}
