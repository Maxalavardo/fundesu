import { Test, TestingModule } from '@nestjs/testing';
import { InstalacionDisciplinaService } from './instalacion-disciplina.service';

describe('InstalacionDisciplinaService', () => {
    let service: InstalacionDisciplinaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [InstalacionDisciplinaService],
        }).compile();

        service = module.get<InstalacionDisciplinaService>(InstalacionDisciplinaService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
