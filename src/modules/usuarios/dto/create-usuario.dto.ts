import { IsBoolean, IsString, IsUUID, IsOptional } from 'class-validator';
import { Clube } from 'src/modules/clubes/entities/clube.entity';
import { Municipio } from 'src/modules/municipios/entities/municipio.entity';
import { Persona } from 'src/modules/personas/entities/persona.entity';

export class CreateUsuarioDto {
    _id?: string;

    @IsString()
    usuario: string;

    @IsString()
    contrasena: string;

    @IsString()
    rol: string;

    @IsBoolean()
    isActive: boolean;

    @IsUUID()
    persona: Persona;

    @IsUUID()
    @IsOptional()
    municipio?: Municipio;

    @IsUUID()
    @IsOptional()
    club?: Clube;
}
