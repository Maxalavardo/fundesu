import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { EquipamientoDepotivoEsp } from '../equipamiento-depotivo-esp/entities/equipamiento-depotivo-esp.entity';

@Injectable()
export class EquipamientoDepotivoEspSeeder implements Seeder {
    constructor(
        @InjectRepository(EquipamientoDepotivoEsp)
        private readonly equipamientoDepotivoEspRepository: Repository<EquipamientoDepotivoEsp>,
    ) {}

    async seed(): Promise<any> {
        const { equipamientosDeportivos } = JSON.parse(readFileSync('./src/modules/seeders/jsons/equipamientosDeportivos.json', 'utf8'));

        try {
            for await (const equipamientoDeportivo of equipamientosDeportivos) {
                await this.equipamientoDepotivoEspRepository.save({ nombreEquipamiento: equipamientoDeportivo });
            }
        } catch (exception: any) {
            console.log('Crear tipo instalacion deportiva: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const equipamientoDeportivo = await this.equipamientoDepotivoEspRepository.find();
        return await this.equipamientoDepotivoEspRepository.remove(equipamientoDeportivo);
    }
}
