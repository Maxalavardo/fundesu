import { TypeOrmModule } from '@nestjs/typeorm';
import 'dotenv/config';

export const connection = TypeOrmModule.forRoot({
    type: 'postgres',
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    entities: ['dist/**/*.entity{.ts,.js}'],
    synchronize: true,
    ssl: process.env.DB_SSL === 'true',
});
