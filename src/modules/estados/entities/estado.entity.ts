import { Municipio } from 'src/modules/municipios/entities/municipio.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Estado {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreEstado: string;

    @Column({ type: Number, generated: 'increment', nullable: true })
    index: number | null;

    @OneToMany(() => Municipio, (municipio) => municipio.estado)
    municipio: Municipio;
}
