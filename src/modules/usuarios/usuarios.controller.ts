import { Controller, Get, Post, Body, Patch, Param, Delete, Request } from '@nestjs/common';
import { UsuariosService } from './usuarios.service';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';

@Controller('usuarios')
@ApiTags('Usuarios')
export class UsuariosController {
    constructor(private readonly usuariosService: UsuariosService) {}

    @AuthAll()
    @Post()
    @ApiBody({ type: [CreateUsuarioDto] })
    create(@Request() req, @Body() createUsuarioDto: CreateUsuarioDto) {
        return this.usuariosService.create(createUsuarioDto, req.user);
    }

    @Get()
    findAll() {
        return this.usuariosService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.usuariosService.findOne(id);
    }

    @Get(':usuario/:contrasena')
    findUser(@Param() params: { usuario: string; contrasena: string }) {
        return this.usuariosService.findUser(params.usuario, params.contrasena);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateUsuarioDto] })
    update(@Param('id') id: string, @Body() updateUsuarioDto: UpdateUsuarioDto) {
        return this.usuariosService.update(id, updateUsuarioDto);
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.usuariosService.remove(id, req.user);
    }
}
