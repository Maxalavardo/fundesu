import { IsInt, IsUUID } from 'class-validator';
import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { Evento } from 'src/modules/eventos/entities/evento.entity';

export class CreateEventosAtletaDto {
    _id?: string;

    @IsUUID()
    atleta: Atleta;

    @IsUUID()
    evento: Evento;

    @IsInt()
    posicion: number;
}
