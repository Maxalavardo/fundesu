import { Test, TestingModule } from '@nestjs/testing';
import { TipoAtletaService } from './tipo-atleta.service';

describe('TipoAtletaService', () => {
    let service: TipoAtletaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [TipoAtletaService],
        }).compile();

        service = module.get<TipoAtletaService>(TipoAtletaService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
