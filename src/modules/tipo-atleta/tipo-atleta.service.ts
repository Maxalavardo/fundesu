import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateTipoAtletaDto } from './dto/create-tipo-atleta.dto';
import { UpdateTipoAtletaDto } from './dto/update-tipo-atleta.dto';
import { TipoAtleta } from './entities/tipo-atleta.entity';

@Injectable()
export class TipoAtletaService {
    constructor(
        @InjectRepository(TipoAtleta)
        private readonly tipoAtletaRepository: Repository<TipoAtleta>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    async create(request: CreateTipoAtletaDto, user: any): Promise<TipoAtleta> {
        let tipoAtleta = this.tipoAtletaRepository.create(request);
        tipoAtleta = await this.tipoAtletaRepository.save(tipoAtleta);

        const message = 'Un tipo de atleta a sido registrado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: TipoAtleta.name,
            uuid: tipoAtleta._id,
            data: tipoAtleta,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return tipoAtleta;
    }

    async findAll() {
        return await this.tipoAtletaRepository.find();
    }

    async findOne(id: string) {
        return await this.tipoAtletaRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateTipoAtletaDto) {
        const upTipoAtleta = await this.tipoAtletaRepository.findOne({ where: { _id: id } });
        this.tipoAtletaRepository.merge(upTipoAtleta, request);
        return await this.tipoAtletaRepository.save(upTipoAtleta);
    }

    async remove(id: string, user: any) {
        const tipoAtleta = await this.tipoAtletaRepository.findOne({ where: { _id: id } });

        const message = 'Un tipo de atleta a sido eliminado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: TipoAtleta.name,
            uuid: id,
            data: tipoAtleta,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.tipoAtletaRepository.softDelete(id);
    }
}
