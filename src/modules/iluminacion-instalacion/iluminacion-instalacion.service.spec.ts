import { Test, TestingModule } from '@nestjs/testing';
import { IluminacionInstalacionService } from './iluminacion-instalacion.service';

describe('IluminacionInstalacionService', () => {
    let service: IluminacionInstalacionService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [IluminacionInstalacionService],
        }).compile();

        service = module.get<IluminacionInstalacionService>(IluminacionInstalacionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
