import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { BecasAtletasService } from './becas-atletas.service';
import { CreateBecasAtletaDto } from './dto/create-becas-atleta.dto';
import { UpdateBecasAtletaDto } from './dto/update-becas-atleta.dto';

@Controller('becas-atletas')
@ApiTags('Becas-atletas')
export class BecasAtletasController {
    constructor(private readonly becasAtletasService: BecasAtletasService) {}

    @Post()
    @ApiBody({ type: [CreateBecasAtletaDto] })
    create(@Res() Res, @Body() createBecasAtletaDto: CreateBecasAtletaDto) {
        return this.becasAtletasService
            .create(createBecasAtletaDto)
            .then((becaAtleta) => {
                return Res.status(HttpStatus.CREATED).json(becaAtleta);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.becasAtletasService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.becasAtletasService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateBecasAtletaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateBecasAtletaDto: UpdateBecasAtletaDto) {
        return this.becasAtletasService
            .update(id, updateBecasAtletaDto)
            .then((becaAtleta) => {
                return Res.status(HttpStatus.CREATED).json(becaAtleta);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.becasAtletasService.remove(id);
    }
}
