import { IsString } from 'class-validator';

export class CreateDisciplinasDeportivaDto {
    _id?: string;

    @IsString()
    nombreDisciplina: string;
}
