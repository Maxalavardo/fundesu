import { MaterialComplemento } from 'src/modules/material-complemento/entities/material-complemento.entity';
import { MaterialEquipamientoEsp } from 'src/modules/material-equipamiento-esp/entities/material-equipamiento-esp.entity';
import { MaterialIluminacion } from 'src/modules/material-iluminacion/entities/material-iluminacion.entity';
import { MaterialPared } from 'src/modules/material-pared/entities/material-pared.entity';
import { MaterialSuperficie } from 'src/modules/material-superficie/entities/material-superficie.entity';
import { MaterialTechado } from 'src/modules/material-techado/entities/material-techado.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Materiale {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreMaterial: string;

    @OneToMany(() => MaterialSuperficie, (superficieMaterial) => superficieMaterial.material)
    superficieMaterial: MaterialSuperficie;

    @OneToMany(() => MaterialPared, (paredMaterial) => paredMaterial.material)
    paredMaterial: MaterialPared;

    @OneToMany(() => MaterialIluminacion, (iluminacionMaterial) => iluminacionMaterial.material)
    iluminacionMaterial: MaterialIluminacion;

    @OneToMany(() => MaterialTechado, (techadoMaterial) => techadoMaterial.material)
    techadoMaterial: MaterialTechado;

    @OneToMany(() => MaterialComplemento, (complementoMaterial) => complementoMaterial.material)
    complementoMaterial: MaterialComplemento;

    @OneToMany(() => MaterialEquipamientoEsp, (equipamientoMaterial) => equipamientoMaterial.material)
    equipamientoMaterial: MaterialEquipamientoEsp;
}
