import { ViewColumn, ViewEntity } from 'typeorm';

@ViewEntity({
    expression: `
    SELECT
    instalaciones_deportiva._id,
    instalaciones_deportiva."nombreInstalacion",
    instalaciones_deportiva."medidaLargo",
    instalaciones_deportiva."medidaAncho",
    instalaciones_deportiva."metrosCuadrados",
    instalaciones_deportiva."tipoEspacio",
    instalaciones_deportiva."estadoInstalacion",
    instalaciones_deportiva."recuperacionInstalacion",
    superficie_instalacion."nombreSuperficie",
    superficie_instalacion."medidaSuperficie",
    techado_instalacion."nombreTechado",
    techado_instalacion."medidaTechado",
    iluminacion_instalacion."nombreIluminacion",
    iluminacion_instalacion."cantidadIluminacion",
    pared_instalacion."nombrePared",
    pared_instalacion."medidaPared",
    complementos_instalacion."nombreComplemento",
    complementos_instalacion."medidaComplemento",
    complementos_instalacion."cantidadComplemento",
    equipamiento_depotivo_esp."nombreEquipamiento",
    equipamiento_instalacion."cantidadEquipamiento",
    implementos_deportivo."nombreImplemento",
    implementos_instalacion."cantidadImplemento"
    FROM 
    instalaciones_deportiva, superficie_instalacion, techado_instalacion, iluminacion_instalacion,
    complementos_instalacion, equipamiento_instalacion, equipamiento_depotivo_esp, pared_instalacion, 
    implementos_instalacion, implementos_deportivo 
    WHERE 
    instalaciones_deportiva._id=superficie_instalacion.instalaciones_id and
    instalaciones_deportiva._id=techado_instalacion.instalaciones_id and
    instalaciones_deportiva._id=iluminacion_instalacion.instalaciones_id and
    instalaciones_deportiva._id=pared_instalacion.instalaciones_id and
    instalaciones_deportiva._id=complementos_instalacion.instalaciones_id and
    instalaciones_deportiva._id=equipamiento_instalacion.instalaciones_id and
    instalaciones_deportiva._id=implementos_instalacion.instalaciones_id and
    implementos_instalacion.implemento_id=implementos_deportivo._id and
    equipamiento_instalacion."equipamientoEsp_id"=equipamiento_depotivo_esp._id;
    `,
})
export class ViewInstalacion {
    @ViewColumn()
    _id: string;

    @ViewColumn()
    nombreInstalacion: string;

    @ViewColumn()
    medidaLargo: number;

    @ViewColumn()
    medidaAncho: number;

    @ViewColumn()
    metrosCuadrados: number;

    @ViewColumn()
    tipoEspacio: string;

    @ViewColumn()
    estadoInstalacion: string;

    @ViewColumn()
    recuperacionInstalacion: boolean;

    @ViewColumn()
    nombreSuperficie: string;

    @ViewColumn()
    medidaSuperficie: number;

    @ViewColumn()
    nombreTechado: string;

    @ViewColumn()
    medidaTechado: number;

    @ViewColumn()
    nombreIluminacion: string;

    @ViewColumn()
    cantidadIluminacion: number;

    @ViewColumn()
    nombrePared: string;

    @ViewColumn()
    medidaPared: number;

    @ViewColumn()
    nombreComplemento: string;

    @ViewColumn()
    medidaComplemento: number;

    @ViewColumn()
    cantidadComplemento: number;

    @ViewColumn()
    nombreEquipamiento: string;

    @ViewColumn()
    cantidadEquipamiento: number;

    @ViewColumn()
    nombreImplemento: string;

    @ViewColumn()
    cantidadImplemento: number;
}
