import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateComplejosClubeDto } from './dto/create-complejos-clube.dto';
import { UpdateComplejosClubeDto } from './dto/update-complejos-clube.dto';
import { ComplejosClube } from './entities/complejos-clube.entity';

@Injectable()
export class ComplejosClubesService {
    constructor(
        @InjectRepository(ComplejosClube)
        private readonly complejoClubRepository: Repository<ComplejosClube>,
    ) {}
    async create(request: CreateComplejosClubeDto) {
        const complejoClub = this.complejoClubRepository.create(request);
        return await this.complejoClubRepository.save(complejoClub);
    }

    async findAll() {
        return await this.complejoClubRepository.find();
    }

    async findOne(id: string) {
        return await this.complejoClubRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateComplejosClubeDto) {
        const upComplejoClub = await this.complejoClubRepository.findOne({ where: { _id: id } });
        upComplejoClub.club = request.club;
        upComplejoClub.complejo = request.complejo;
        this.complejoClubRepository.merge(upComplejoClub, request);
        return await this.complejoClubRepository.save(upComplejoClub);
    }

    async remove(id: string) {
        return await this.complejoClubRepository.delete(id);
    }
}
