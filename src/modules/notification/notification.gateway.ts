// import { WebSocketGateway, SubscribeMessage, MessageBody } from '@nestjs/websockets';
import { NotificationService } from './notification.service';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { UpdateNotificationDto } from './dto/update-notification.dto';

// @WebSocketGateway()
export class NotificationGateway {
    constructor(private readonly notificationService: NotificationService) {}

    // @SubscribeMessage('createNotification')
    create(createNotificationDto: CreateNotificationDto) {
        return this.notificationService.create(createNotificationDto);
    }

    // @SubscribeMessage('findAllNotification')
    findAll() {
        return this.notificationService.findAll();
    }

    // @SubscribeMessage('findOneNotification')
    findOne(id: string) {
        return this.notificationService.findOne(id);
    }

    // @SubscribeMessage('updateNotification')
    update(updateNotificationDto: UpdateNotificationDto) {
        return this.notificationService.update(updateNotificationDto._id, updateNotificationDto);
    }

    // @SubscribeMessage('removeNotification')
    remove(id: number) {
        return this.notificationService.remove(id);
    }
}
