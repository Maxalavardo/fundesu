import { PartialType } from '@nestjs/mapped-types';
import { CreateMaterialParedDto } from './create-material-pared.dto';

export class UpdateMaterialParedDto extends PartialType(CreateMaterialParedDto) {}
