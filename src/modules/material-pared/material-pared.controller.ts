import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { MaterialParedService } from './material-pared.service';
import { CreateMaterialParedDto } from './dto/create-material-pared.dto';
import { UpdateMaterialParedDto } from './dto/update-material-pared.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('material-pared')
@ApiTags('Material-pared')
export class MaterialParedController {
    constructor(private readonly materialParedService: MaterialParedService) {}

    @Post()
    @ApiBody({ type: [CreateMaterialParedDto] })
    create(@Res() Res, @Body() createMaterialParedDto: CreateMaterialParedDto) {
        this.materialParedService
            .create(createMaterialParedDto)
            .then((pared) => {
                return Res.status(HttpStatus.CREATED).json(pared);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.materialParedService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.materialParedService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateMaterialParedDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateMaterialParedDto: UpdateMaterialParedDto) {
        this.materialParedService
            .update(id, updateMaterialParedDto)
            .then((pared) => {
                return Res.status(HttpStatus.ACCEPTED).json(pared);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.materialParedService.remove(id);
    }
}
