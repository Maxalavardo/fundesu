import { IsInt, IsUUID } from 'class-validator';
import { Materiale } from 'src/modules/materiales/entities/materiale.entity';
import { TechadoInstalacion } from 'src/modules/techado-instalacion/entities/techado-instalacion.entity';

export class CreateMaterialTechadoDto {
    _id?: string;

    @IsInt()
    cantidadMaterial: number;

    @IsUUID()
    material: Materiale;

    @IsUUID()
    techado: TechadoInstalacion;
}
