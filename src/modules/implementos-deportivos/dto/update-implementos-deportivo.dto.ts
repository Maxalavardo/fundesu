import { PartialType } from '@nestjs/mapped-types';
import { CreateImplementosDeportivoDto } from './create-implementos-deportivo.dto';

export class UpdateImplementosDeportivoDto extends PartialType(CreateImplementosDeportivoDto) {}
