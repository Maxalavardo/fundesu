import { PartialType } from '@nestjs/mapped-types';
import { CreateDiscapacidadesAtletaDto } from './create-discapacidades-atleta.dto';

export class UpdateDiscapacidadesAtletaDto extends PartialType(CreateDiscapacidadesAtletaDto) {}
