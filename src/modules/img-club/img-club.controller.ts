import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ImgClubService } from './img-club.service';
import { CreateImgClubDto } from './dto/create-img-club.dto';
import { UpdateImgClubDto } from './dto/update-img-club.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('img-club')
@ApiTags('Img-club')
export class ImgClubController {
    constructor(private readonly imgClubService: ImgClubService) {}

    @Post()
    @ApiBody({ type: [CreateImgClubDto] })
    create(@Res() Res, @Body() createImgClubDto: CreateImgClubDto) {
        this.imgClubService.create(createImgClubDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Get()
    findAll() {
        return this.imgClubService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.imgClubService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImgClubDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateImgClubDto: UpdateImgClubDto) {
        this.imgClubService.update(id, updateImgClubDto).then((img) => {
            return Res.status(HttpStatus.ACCEPTED).json(img);
        });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.imgClubService.remove(id);
    }
}
