import { Module } from '@nestjs/common';
import { InstalacionesDeportivasService } from './instalaciones-deportivas.service';
import { InstalacionesDeportivasController } from './instalaciones-deportivas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InstalacionesDeportiva } from './entities/instalaciones-deportiva.entity';
import { ViewInstalacion } from './entities/viewInstalaciones.entity';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';

@Module({
    imports: [TypeOrmModule.forFeature([InstalacionesDeportiva, ViewInstalacion, Notification, Usuario])],
    controllers: [InstalacionesDeportivasController],
    providers: [InstalacionesDeportivasService],
})
export class InstalacionesDeportivasModule {}
