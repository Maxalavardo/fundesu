import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { ApiBearerAuth } from '@nestjs/swagger';
import { UserRoleEnum } from 'src/modules/usuarios/enums/user-rol.enum';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { RolesGuard } from '../guards/role.guard';
export const ROLES_KEY = 'roles';

/**
 * Decorador que permite verificar la autenticacion segun un rol
 *
 * @constructor
 */
export function Auth(...roles: UserRoleEnum[]) {
    return applyDecorators(SetMetadata(ROLES_KEY, roles), ApiBearerAuth(), UseGuards(JwtAuthGuard, RolesGuard));
}

/**
 * Decorador que permite verificar la autenticacion para todos los roles
 *
 * @constructor
 */
export function AuthAll() {
    const roles = [
        UserRoleEnum.SUPER_ADMIN,
        UserRoleEnum.IDES_ADMIN,
        UserRoleEnum.CLUB_ADMIN,
        UserRoleEnum.INSTALACION_ADMIN,
    ];
    return applyDecorators(SetMetadata(ROLES_KEY, roles), ApiBearerAuth(), UseGuards(JwtAuthGuard, RolesGuard));
}
