import { Test, TestingModule } from '@nestjs/testing';
import { MaterialTechadoService } from './material-techado.service';

describe('MaterialTechadoService', () => {
    let service: MaterialTechadoService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MaterialTechadoService],
        }).compile();

        service = module.get<MaterialTechadoService>(MaterialTechadoService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
