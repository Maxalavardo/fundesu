import { AtletasClub } from 'src/modules/atletas-club/entities/atletas-club.entity';
import { ComplejosClube } from 'src/modules/complejos-clubes/entities/complejos-clube.entity';
import { DisciplinasDeportiva } from 'src/modules/disciplinas-deportivas/entities/disciplinas-deportiva.entity';
import { ImgClub } from 'src/modules/img-club/entities/img-club.entity';
import { MiembrosClube } from 'src/modules/miembros-clubes/entities/miembros-clube.entity';
import { Usuario } from 'src/modules/usuarios/entities/usuario.entity';
import {
    Column,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    DeleteDateColumn,
    OneToMany,
} from 'typeorm';

@Entity()
export class Clube {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar', unique: true })
    nombreClub: string;

    @ManyToOne(() => DisciplinasDeportiva, (disciplina) => disciplina.club, {
        eager: true,
    })
    disciplina: DisciplinasDeportiva;

    @Column({ type: 'date' })
    fechaCreacion: Date;

    @CreateDateColumn({
        type: 'timestamp',
        default: () => 'CURRENT_TIMESTAMP(6)',
    })
    created_at: Date;

    @DeleteDateColumn()
    deletedAt?: Date;

    @OneToMany(() => MiembrosClube, (miembroClub) => miembroClub.club)
    miembroClub: MiembrosClube;

    @OneToMany(() => AtletasClub, (atletaClub) => atletaClub.club)
    atletaClub: AtletasClub;

    @OneToMany(() => ComplejosClube, (complejoClub) => complejoClub.club)
    complejoClub: ComplejosClube;

    @OneToMany(() => Usuario, (usuario) => usuario.club)
    usuario: Usuario;

    @OneToMany(() => ImgClub, (imageClub) => imageClub.club)
    imageClub: ImgClub;
}
