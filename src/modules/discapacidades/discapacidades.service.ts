import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateDiscapacidadeDto } from './dto/create-discapacidade.dto';
import { UpdateDiscapacidadeDto } from './dto/update-discapacidade.dto';
import { Discapacidade } from './entities/discapacidade.entity';

@Injectable()
export class DiscapacidadesService {
    constructor(
        @InjectRepository(Discapacidade)
        private readonly discapacidadRepository: Repository<Discapacidade>,
    ) {}

    async create(request: CreateDiscapacidadeDto) {
        const discapacidad = await this.discapacidadRepository.create(request);
        return await this.discapacidadRepository.save(discapacidad);
    }

    async findAll() {
        return await this.discapacidadRepository.find();
    }

    async findOne(id: string) {
        return await this.discapacidadRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateDiscapacidadeDto) {
        const upDiscapacidad = await this.discapacidadRepository.findOne({ where: { _id: id } });
        await this.discapacidadRepository.merge(upDiscapacidad, request);
        return await this.discapacidadRepository.save(upDiscapacidad);
    }

    async remove(id: string) {
        return await this.discapacidadRepository.softDelete(id);
    }
}
