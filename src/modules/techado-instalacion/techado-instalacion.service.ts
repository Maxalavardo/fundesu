import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTechadoInstalacionDto } from './dto/create-techado-instalacion.dto';
import { UpdateTechadoInstalacionDto } from './dto/update-techado-instalacion.dto';
import { TechadoInstalacion } from './entities/techado-instalacion.entity';

@Injectable()
export class TechadoInstalacionService {
    constructor(
        @InjectRepository(TechadoInstalacion)
        private readonly techadoInsRepository: Repository<TechadoInstalacion>,
    ) {}

    async create(request: CreateTechadoInstalacionDto) {
        const techado = await this.techadoInsRepository.create(request);
        return await this.techadoInsRepository.save(techado);
    }

    async findAll() {
        return await this.techadoInsRepository.find();
    }

    async findOne(id: string) {
        const complemento = await this.techadoInsRepository
            .createQueryBuilder('i')
            .where('instalaciones_id = :id', { id });
        return await complemento.getMany();
    }

    async update(id: string, request: UpdateTechadoInstalacionDto) {
        const upTechado = await this.techadoInsRepository.findOne({ where: { _id: id } });
        upTechado.instalaciones = request.instalaciones;
        await this.techadoInsRepository.merge(upTechado, request);
        return await this.techadoInsRepository.save(upTechado);
    }

    async remove(id: string) {
        return await this.techadoInsRepository.delete(id);
    }
}
