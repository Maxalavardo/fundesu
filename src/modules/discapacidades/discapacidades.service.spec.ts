import { Test, TestingModule } from '@nestjs/testing';
import { DiscapacidadesService } from './discapacidades.service';

describe('DiscapacidadesService', () => {
    let service: DiscapacidadesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [DiscapacidadesService],
        }).compile();

        service = module.get<DiscapacidadesService>(DiscapacidadesService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
