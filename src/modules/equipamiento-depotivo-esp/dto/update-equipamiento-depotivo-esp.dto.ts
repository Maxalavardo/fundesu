import { PartialType } from '@nestjs/mapped-types';
import { CreateEquipamientoDepotivoEspDto } from './create-equipamiento-depotivo-esp.dto';

export class UpdateEquipamientoDepotivoEspDto extends PartialType(CreateEquipamientoDepotivoEspDto) {}
