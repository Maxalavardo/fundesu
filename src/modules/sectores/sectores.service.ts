import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSectoreDto } from './dto/create-sectore.dto';
import { UpdateSectoreDto } from './dto/update-sectore.dto';
import { Sector } from './entities/sectore.entity';

@Injectable()
export class SectoresService {
    constructor(
        @InjectRepository(Sector)
        private readonly sectorRespository: Repository<Sector>,
    ) {}

    async create(request: CreateSectoreDto): Promise<Sector> {
        const sector = this.sectorRespository.create(request);
        return this.sectorRespository.save(sector);
    }

    async findAll(comunidadId?: string) {
        const sectores = this.sectorRespository
            .createQueryBuilder('i')
            .innerJoinAndSelect('i.comunidad', 'comunidad')
            .innerJoinAndSelect('comunidad.parroquia', 'parroquia')
            .innerJoinAndSelect('parroquia.municipio', 'municipio')
            .innerJoinAndSelect('municipio.estado', 'estado')
            .where('1 = 1');

        if (comunidadId) {
            sectores.andWhere('comunidad._id = :comunidadId', { comunidadId });
        }

        return await sectores.getMany();
    }

    async findOne(_id: string): Promise<Sector> {
        return await this.sectorRespository.findOne({ where: { _id } });
    }

    async update(id: string, request: UpdateSectoreDto) {
        const upSector = await this.sectorRespository.findOne({ where: { _id: id } });
        upSector.comunidad = request.comunidad;
        this.sectorRespository.merge(upSector, request);
        return await this.sectorRespository.save(upSector);
    }

    async remove(id: string) {
        return await this.sectorRespository.softDelete(id);
    }
}
