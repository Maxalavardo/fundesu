import { Test, TestingModule } from '@nestjs/testing';
import { ImgComplejoDepService } from './img-complejo-dep.service';

describe('ImgComplejoDepService', () => {
    let service: ImgComplejoDepService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImgComplejoDepService],
        }).compile();

        service = module.get<ImgComplejoDepService>(ImgComplejoDepService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
