import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Request } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';
import { ClubesService } from './clubes.service';
import { CreateClubeDto } from './dto/create-clube.dto';
import { UpdateClubeDto } from './dto/update-clube.dto';

@Controller('clubes')
@ApiTags('Clubes')
export class ClubesController {
    constructor(private readonly clubesService: ClubesService) {}

    @AuthAll()
    @Post()
    @ApiBody({ type: [CreateClubeDto] })
    create(@Request() req, @Res() Res, @Body() createClubeDto: CreateClubeDto) {
        return this.clubesService
            .create(createClubeDto, req.user)
            .then((club) => {
                return Res.status(HttpStatus.CREATED).json(club);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.clubesService.findAll();
    }

    @Get('total-clubes')
    countAll() {
        return this.clubesService.count();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.clubesService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateClubeDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateClubeDto: UpdateClubeDto) {
        return this.clubesService
            .update(id, updateClubeDto)
            .then((club) => {
                return Res.status(HttpStatus.ACCEPTED).json(club);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.clubesService.remove(id, req.user);
    }
}
