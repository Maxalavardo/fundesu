import { Test, TestingModule } from '@nestjs/testing';
import { ImgParedController } from './img-pared.controller';
import { ImgParedService } from './img-pared.service';

describe('ImgParedController', () => {
    let controller: ImgParedController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImgParedController],
            providers: [ImgParedService],
        }).compile();

        controller = module.get<ImgParedController>(ImgParedController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
