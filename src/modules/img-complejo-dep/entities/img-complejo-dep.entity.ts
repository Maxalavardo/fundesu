import { ComplejosDeportivo } from 'src/modules/complejos-deportivos/entities/complejos-deportivo.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ImgComplejoDep {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'text', nullable: true })
    image: string;

    @ManyToOne(() => ComplejosDeportivo, (complejo) => complejo.imageComplejo, {
        eager: true,
    })
    complejo: ComplejosDeportivo;
}
