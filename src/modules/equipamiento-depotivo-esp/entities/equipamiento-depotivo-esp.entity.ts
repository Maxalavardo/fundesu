import { EquipamientoInstalacion } from 'src/modules/equipamiento-instalacion/entities/equipamiento-instalacion.entity';
import { MaterialEquipamientoEsp } from 'src/modules/material-equipamiento-esp/entities/material-equipamiento-esp.entity';
import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class EquipamientoDepotivoEsp {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreEquipamiento: string;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => EquipamientoInstalacion, (equipamiento) => equipamiento.equipamientoEsp)
    equipamiento: EquipamientoInstalacion;

    @OneToMany(() => MaterialEquipamientoEsp, (equipamientoMaterial) => equipamientoMaterial.equipamiento)
    equipamientoMaterial: MaterialEquipamientoEsp;
}
