import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TipoAtleta {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreTipoAtleta: string;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => Atleta, (atleta) => atleta.tipoAtleta)
    atleta: Atleta;
}
