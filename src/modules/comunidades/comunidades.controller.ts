import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Query, Request } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';
import { ComunidadesService } from './comunidades.service';
import { CreateComunidadeDto } from './dto/create-comunidade.dto';
import { UpdateComunidadeDto } from './dto/update-comunidade.dto';

@Controller('comunidades')
@ApiTags('Comunidades')
export class ComunidadesController {
    constructor(private readonly comunidadesService: ComunidadesService) {}

    @AuthAll()
    @Post()
    @ApiBody({ type: [CreateComunidadeDto] })
    create(@Request() req, @Res() Res, @Body() createComunidadeDto: CreateComunidadeDto) {
        this.comunidadesService
            .create(createComunidadeDto, req.user)
            .then((comunidad) => {
                return Res.status(HttpStatus.CREATED).json(comunidad);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    async findAll(@Query('parroquiaId') parroquiaId: string) {
        return await this.comunidadesService.findAll(parroquiaId);
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.comunidadesService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateComunidadeDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateComunidadeDto: UpdateComunidadeDto) {
        this.comunidadesService
            .update(id, updateComunidadeDto)
            .then((comunidad) => {
                return Res.status(HttpStatus.ACCEPTED).json(comunidad);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.comunidadesService.remove(id, req.user);
    }
}
