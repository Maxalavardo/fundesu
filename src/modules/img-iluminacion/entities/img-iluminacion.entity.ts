import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ImgIluminacion {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'text', nullable: true })
    image: string;

    @ManyToOne(() => InstalacionesDeportiva, (instalaciones) => instalaciones.imageIluminacion, { eager: true })
    instalaciones: InstalacionesDeportiva;
}
