import { PartialType } from '@nestjs/mapped-types';
import { CreateIluminacionInstalacionDto } from './create-iluminacion-instalacion.dto';

export class UpdateIluminacionInstalacionDto extends PartialType(CreateIluminacionInstalacionDto) {}
