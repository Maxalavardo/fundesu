export class JWTPayload {
    _id: string;
    usuario: string;
    createdAt: Date;
}
