import { AlturaPesoAtleta } from 'src/modules/altura-peso-atleta/entities/altura-peso-atleta.entity';
import { AtletasClub } from 'src/modules/atletas-club/entities/atletas-club.entity';
import { BecasAtleta } from 'src/modules/becas-atletas/entities/becas-atleta.entity';
import { DiscapacidadesAtleta } from 'src/modules/discapacidades-atleta/entities/discapacidades-atleta.entity';
import { DisciplinasAtleta } from 'src/modules/disciplinas-atleta/entities/disciplinas-atleta.entity';
import { EventosAtleta } from 'src/modules/eventos-atletas/entities/eventos-atleta.entity';
import { Persona } from 'src/modules/personas/entities/persona.entity';
import { TallaAtleta } from 'src/modules/talla-atleta/entities/talla-atleta.entity';
import { TipoAtleta } from 'src/modules/tipo-atleta/entities/tipo-atleta.entity';
import {
    Column,
    DeleteDateColumn,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Atleta {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    tipoSangre: string;

    @OneToOne(() => Persona, { eager: true })
    @JoinColumn({ name: 'persona_id' })
    persona: Persona;

    @ManyToOne(() => TipoAtleta, (tipoAtleta) => tipoAtleta.atleta, { eager: true })
    tipoAtleta: TipoAtleta;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => TallaAtleta, (tallaAtleta) => tallaAtleta.atleta)
    tallaAtleta: TallaAtleta;

    @OneToMany(() => AlturaPesoAtleta, (alturaPesoAtleta) => alturaPesoAtleta.atleta)
    alturaPesoAtleta: AlturaPesoAtleta;

    @OneToMany(() => BecasAtleta, (becasAtleta) => becasAtleta.atleta)
    becaAtleta: BecasAtleta;

    @OneToMany(() => DiscapacidadesAtleta, (discapacidadAtleta) => discapacidadAtleta.atleta)
    discapacidadAtleta: DiscapacidadesAtleta;

    @OneToMany(() => DisciplinasAtleta, (disciplinadAtleta) => disciplinadAtleta.atleta)
    disciplinadAtleta: DisciplinasAtleta;

    @OneToMany(() => EventosAtleta, (evento) => evento.atleta)
    evento: EventosAtleta;

    @OneToMany(() => AtletasClub, (atletaClub) => atletaClub.atleta)
    atletaClub: AtletasClub;
}
