import { Comunidades } from 'src/modules/comunidades/entities/comunidade.entity';
import { Municipio } from 'src/modules/municipios/entities/municipio.entity';
import { Column, DeleteDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Parroquia {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreParroquia: string;

    @Column({ nullable: true })
    @DeleteDateColumn()
    deleteAt?: Date;

    @ManyToOne(() => Municipio, (municipio) => municipio.parroquia, {
        eager: true,
    })
    municipio: Municipio;

    @OneToMany(() => Comunidades, (comunidad) => comunidad.parroquia)
    comunidad: Comunidades;
}
