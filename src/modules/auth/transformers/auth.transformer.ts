import { Usuario } from 'src/modules/usuarios/entities/usuario.entity';
import { UserTransformer } from 'src/modules/usuarios/transformers/user.transformer';
import { IToken } from '../interfaces/itoken.interfaces';

export class AuthTransformer {
    public static transform(token: IToken) {
        const user: Usuario = token.getUser();

        return {
            user: UserTransformer.transform(user),
            token: token.getHash(),
        };
    }
}
