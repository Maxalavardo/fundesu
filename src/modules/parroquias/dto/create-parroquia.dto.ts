import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { Municipio } from 'src/modules/municipios/entities/municipio.entity';

export class CreateParroquiaDto {
    _id?: string;

    @IsNotEmpty()
    @IsString()
    nombreParroquia: string;

    @IsUUID()
    municipio: Municipio;
}
