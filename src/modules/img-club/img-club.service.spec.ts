import { Test, TestingModule } from '@nestjs/testing';
import { ImgClubService } from './img-club.service';

describe('ImgClubService', () => {
    let service: ImgClubService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImgClubService],
        }).compile();

        service = module.get<ImgClubService>(ImgClubService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
