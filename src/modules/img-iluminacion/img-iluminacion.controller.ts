import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ImgIluminacionService } from './img-iluminacion.service';
import { CreateImgIluminacionDto } from './dto/create-img-iluminacion.dto';
import { UpdateImgIluminacionDto } from './dto/update-img-iluminacion.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('img-iluminacion')
@ApiTags('Img-iluminacion')
export class ImgIluminacionController {
    constructor(private readonly imgIluminacionService: ImgIluminacionService) {}

    @Post()
    @ApiBody({ type: [CreateImgIluminacionDto] })
    create(@Res() Res, @Body() createImgIluminacionDto: CreateImgIluminacionDto) {
        this.imgIluminacionService.create(createImgIluminacionDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Get()
    findAll() {
        return this.imgIluminacionService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.imgIluminacionService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImgIluminacionDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateImgIluminacionDto: UpdateImgIluminacionDto) {
        this.imgIluminacionService.update(id, updateImgIluminacionDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.imgIluminacionService.remove(id);
    }
}
