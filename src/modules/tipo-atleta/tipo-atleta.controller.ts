import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Request } from '@nestjs/common';
import { TipoAtletaService } from './tipo-atleta.service';
import { CreateTipoAtletaDto } from './dto/create-tipo-atleta.dto';
import { UpdateTipoAtletaDto } from './dto/update-tipo-atleta.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';

@Controller('tipo-atleta')
@ApiTags('Tipo-atleta')
export class TipoAtletaController {
    constructor(private readonly tipoAtletaService: TipoAtletaService) {}

    @AuthAll()
    @Post()
    @ApiBody({ type: [CreateTipoAtletaDto] })
    create(@Request() req, @Res() Res, @Body() createTipoAtletaDto: CreateTipoAtletaDto) {
        this.tipoAtletaService
            .create(createTipoAtletaDto, req.user)
            .then((tipo) => {
                return Res.status(HttpStatus.CREATED).json(tipo);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @Get()
    findAll() {
        return this.tipoAtletaService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.tipoAtletaService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateTipoAtletaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateTipoAtletaDto: UpdateTipoAtletaDto) {
        return this.tipoAtletaService
            .update(id, updateTipoAtletaDto)
            .then((tipo) => {
                return Res.status(HttpStatus.ACCEPTED).json(tipo);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.tipoAtletaService.remove(id, req.user);
    }
}
