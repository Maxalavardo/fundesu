import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAlturaPesoAtletaDto } from './dto/create-altura-peso-atleta.dto';
import { UpdateAlturaPesoAtletaDto } from './dto/update-altura-peso-atleta.dto';
import { AlturaPesoAtleta } from './entities/altura-peso-atleta.entity';

@Injectable()
export class AlturaPesoAtletaService {
    constructor(
        @InjectRepository(AlturaPesoAtleta)
        private readonly alturaPesoAtleRepository: Repository<AlturaPesoAtleta>,
    ) {}

    async create(request: CreateAlturaPesoAtletaDto) {
        const alturaPeso = await this.alturaPesoAtleRepository.create(request);
        return await this.alturaPesoAtleRepository.save(alturaPeso);
    }

    async findAll() {
        return await this.alturaPesoAtleRepository.find();
    }

    async findOne(id: string) {
        const complemento = this.alturaPesoAtleRepository
            .createQueryBuilder('i')
            .leftJoinAndSelect('i.atleta', 'atleta')
            .where('atleta._id = :id', { id })
            .orderBy('i.createdAt', 'DESC');

        return await complemento.getMany();
    }

    async update(id: string, request: UpdateAlturaPesoAtletaDto) {
        const upAtleta = await this.alturaPesoAtleRepository.findOne({ where: { _id: id } });
        this.alturaPesoAtleRepository.merge(upAtleta, request);
        return await this.alturaPesoAtleRepository.save(upAtleta);
    }

    async remove(id: string) {
        return await this.alturaPesoAtleRepository.delete(id);
    }
}
