import { Module } from '@nestjs/common';
import { TipoAtletaService } from './tipo-atleta.service';
import { TipoAtletaController } from './tipo-atleta.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoAtleta } from './entities/tipo-atleta.entity';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';

@Module({
    imports: [TypeOrmModule.forFeature([TipoAtleta, Notification, Usuario])],
    controllers: [TipoAtletaController],
    providers: [TipoAtletaService],
})
export class TipoAtletaModule {}
