import { IsString, IsUUID } from 'class-validator';
import { Persona } from 'src/modules/personas/entities/persona.entity';
import { TipoAtleta } from 'src/modules/tipo-atleta/entities/tipo-atleta.entity';

export class CreateAtletaDto {
    _id?: string;

    @IsString()
    tipoSangre: string;

    @IsUUID()
    persona: Persona;

    @IsUUID()
    tipoAtleta: TipoAtleta;
}
