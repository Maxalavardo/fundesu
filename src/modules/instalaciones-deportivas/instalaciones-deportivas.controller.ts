import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Request } from '@nestjs/common';
import { InstalacionesDeportivasService } from './instalaciones-deportivas.service';
import { CreateInstalacionesDeportivaDto } from './dto/create-instalaciones-deportiva.dto';
import { UpdateInstalacionesDeportivaDto } from './dto/update-instalaciones-deportiva.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';

@Controller('instalaciones-deportivas')
@ApiTags('Instalaciones-deportivas')
export class InstalacionesDeportivasController {
    constructor(private readonly instalacionesDeportivasService: InstalacionesDeportivasService) {}

    @AuthAll()
    @Post('/')
    @ApiBody({ type: [CreateInstalacionesDeportivaDto] })
    create(@Request() req, @Res() Res, @Body() createInstalacionesDeportivaDto: CreateInstalacionesDeportivaDto) {
        this.instalacionesDeportivasService.create(createInstalacionesDeportivaDto, req.user).then((instalacion) => {
            return Res.status(HttpStatus.CREATED).json(instalacion);
        });
    }

    @Get()
    findAll() {
        return this.instalacionesDeportivasService.findAll();
    }

    @Get('/view/:id')
    view(@Param('id') id: string) {
        return this.instalacionesDeportivasService.viewInstlaciones(id);
    }

    @Get('/total-instalaciones')
    countAll() {
        return this.instalacionesDeportivasService.count();
    }

    @Get('/recuperacion')
    recuperacionCount() {
        return this.instalacionesDeportivasService.recuperacionCount();
    }

    @Get('/status')
    statusCount() {
        return this.instalacionesDeportivasService.statusCount();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.instalacionesDeportivasService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateInstalacionesDeportivaDto] })
    update(
        @Res() Res,
        @Param('id') id: string,
        @Body() updateInstalacionesDeportivaDto: UpdateInstalacionesDeportivaDto,
    ) {
        this.instalacionesDeportivasService
            .update(id, updateInstalacionesDeportivaDto)
            .then((instalacion) => {
                return Res.status(HttpStatus.ACCEPTED).json(instalacion);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.instalacionesDeportivasService.remove(id, req.user);
    }
}
