import { EventosAtleta } from 'src/modules/eventos-atletas/entities/eventos-atleta.entity';
import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Evento {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreEvento: string;

    @Column({ type: 'date' })
    fechaEvento: Date;

    @Column({ type: 'varchar' })
    direccion: string;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => EventosAtleta, (evento) => evento.evento)
    evento: EventosAtleta;
}
