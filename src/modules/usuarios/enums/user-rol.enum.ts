export enum UserRoleEnum {
    SUPER_ADMIN = 'Super Usuario',
    IDES_ADMIN = 'Administrador IDES',
    CLUB_ADMIN = 'Administrador de club deportivo',
    INSTALACION_ADMIN = 'Administrador de instalaciones deportivas',
}
