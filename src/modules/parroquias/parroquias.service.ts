import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateParroquiaDto } from './dto/create-parroquia.dto';
import { UpdateParroquiaDto } from './dto/update-parroquia.dto';
import { Parroquia } from './entities/parroquia.entity';

@Injectable()
export class ParroquiasService {
    constructor(
        @InjectRepository(Parroquia)
        private readonly parroquiaRepository: Repository<Parroquia>,
    ) {}

    create(request: CreateParroquiaDto): Promise<Parroquia> {
        const parroquia = this.parroquiaRepository.create(request);
        return this.parroquiaRepository.save(parroquia);
    }

    async findAll(municipioId?: string) {
        const parroquias = this.parroquiaRepository
            .createQueryBuilder('i')
            .innerJoinAndSelect('i.municipio', 'municipio')
            .innerJoinAndSelect('municipio.estado', 'estado')
            .where('1 = 1');

        if (municipioId) {
            parroquias.andWhere('municipio._id = :municipioId', { municipioId });
        }

        return await parroquias.getMany();
    }

    async findOne(_id: string) {
        return await this.parroquiaRepository.findOne({ where: { _id } });
    }

    async update(id: string, request: UpdateParroquiaDto) {
        const upParroquia = await this.parroquiaRepository.findOne({ where: { _id: id } });
        upParroquia.municipio = request.municipio;
        await this.parroquiaRepository.merge(upParroquia, request);
        return await this.parroquiaRepository.save(upParroquia);
    }

    async remove(id: string) {
        return await this.parroquiaRepository.softDelete(id);
    }
}
