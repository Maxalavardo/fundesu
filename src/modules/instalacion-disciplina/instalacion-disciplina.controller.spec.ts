import { Test, TestingModule } from '@nestjs/testing';
import { InstalacionDisciplinaController } from './instalacion-disciplina.controller';
import { InstalacionDisciplinaService } from './instalacion-disciplina.service';

describe('InstalacionDisciplinaController', () => {
    let controller: InstalacionDisciplinaController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [InstalacionDisciplinaController],
            providers: [InstalacionDisciplinaService],
        }).compile();

        controller = module.get<InstalacionDisciplinaController>(InstalacionDisciplinaController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
