import { IsString } from 'class-validator';

export class CreateNotificationDto {
    _id?: string;

    @IsString()
    notification: string;
}
