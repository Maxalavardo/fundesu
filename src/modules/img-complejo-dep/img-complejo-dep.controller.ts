import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ImgComplejoDepService } from './img-complejo-dep.service';
import { CreateImgComplejoDepDto } from './dto/create-img-complejo-dep.dto';
import { UpdateImgComplejoDepDto } from './dto/update-img-complejo-dep.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('img-complejo-dep')
@ApiTags('Img-complejo-deportivo')
export class ImgComplejoDepController {
    constructor(private readonly imgComplejoDepService: ImgComplejoDepService) {}

    @Post()
    @ApiBody({ type: [CreateImgComplejoDepDto] })
    create(@Res() Res, @Body() createImgComplejoDepDto: CreateImgComplejoDepDto) {
        this.imgComplejoDepService.create(createImgComplejoDepDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Get()
    findAll() {
        return this.imgComplejoDepService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.imgComplejoDepService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImgComplejoDepDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateImgComplejoDepDto: UpdateImgComplejoDepDto) {
        this.imgComplejoDepService.update(id, updateImgComplejoDepDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.imgComplejoDepService.remove(id);
    }
}
