import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateTipoInstalacionDepDto } from './dto/create-tipo-instalacion-dep.dto';
import { UpdateTipoInstalacionDepDto } from './dto/update-tipo-instalacion-dep.dto';
import { TipoInstalacionDep } from './entities/tipo-instalacion-dep.entity';

@Injectable()
export class TipoInstalacionDepService {
    constructor(
        @InjectRepository(TipoInstalacionDep)
        private readonly tipoInstalacionDepRepository: Repository<TipoInstalacionDep>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    async create(request: CreateTipoInstalacionDepDto, user: any): Promise<TipoInstalacionDep> {
        let tipoInstalacion = this.tipoInstalacionDepRepository.create(request);
        tipoInstalacion = await this.tipoInstalacionDepRepository.save(tipoInstalacion);

        const message = 'Un tipo de instalación deportiva a sido registrada';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: TipoInstalacionDep.name,
            uuid: tipoInstalacion._id,
            data: tipoInstalacion,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return tipoInstalacion;
    }

    async findAll() {
        return await this.tipoInstalacionDepRepository.find();
    }

    async findOne(id: string) {
        return await this.tipoInstalacionDepRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateTipoInstalacionDepDto): Promise<TipoInstalacionDep> {
        const upTipoInstalacion = await this.tipoInstalacionDepRepository.findOne({ where: { _id: id } });
        await this.tipoInstalacionDepRepository.merge(upTipoInstalacion, request);
        return await this.tipoInstalacionDepRepository.save(upTipoInstalacion);
    }

    async remove(id: string, user: any) {
        const tipoInstalacion = await this.tipoInstalacionDepRepository.findOne({ where: { _id: id } });

        const message = 'Un tipo de instalación deportiva a sido eliminada';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: TipoInstalacionDep.name,
            uuid: id,
            data: tipoInstalacion,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.tipoInstalacionDepRepository.softDelete(id);
    }
}
