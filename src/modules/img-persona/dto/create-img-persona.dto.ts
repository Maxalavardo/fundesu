import { IsString, IsUUID } from 'class-validator';
import { Persona } from 'src/modules/personas/entities/persona.entity';

export class CreateImgPersonaDto {
    _id?: string;

    @IsString()
    image?: string;

    @IsUUID()
    persona: Persona;
}
