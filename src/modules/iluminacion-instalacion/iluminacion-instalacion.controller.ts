import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Res } from '@nestjs/common';
import { IluminacionInstalacionService } from './iluminacion-instalacion.service';
import { CreateIluminacionInstalacionDto } from './dto/create-iluminacion-instalacion.dto';
import { UpdateIluminacionInstalacionDto } from './dto/update-iluminacion-instalacion.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('iluminacion-instalacion')
@ApiTags('Iluminacion-instalacion')
export class IluminacionInstalacionController {
    constructor(private readonly iluminacionInstalacionService: IluminacionInstalacionService) {}

    @Post()
    @ApiBody({ type: [CreateIluminacionInstalacionDto] })
    create(@Res() Res, @Body() createIluminacionInstalacionDto: CreateIluminacionInstalacionDto) {
        this.iluminacionInstalacionService
            .create(createIluminacionInstalacionDto)
            .then((iluminacion) => {
                return Res.status(HttpStatus.CREATED).json(iluminacion);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.iluminacionInstalacionService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.iluminacionInstalacionService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateIluminacionInstalacionDto] })
    update(
        @Res() Res,
        @Param('id') id: string,
        @Body() updateIluminacionInstalacionDto: UpdateIluminacionInstalacionDto,
    ) {
        this.iluminacionInstalacionService
            .update(id, updateIluminacionInstalacionDto)
            .then((iluminacion) => {
                return Res.status(HttpStatus.ACCEPTED).json(iluminacion);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.iluminacionInstalacionService.remove(id);
    }
}
