import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Evento } from '../eventos/entities/evento.entity';
import { CreateEventosAtletaDto } from './dto/create-eventos-atleta.dto';
import { UpdateEventosAtletaDto } from './dto/update-eventos-atleta.dto';
import { EventosAtleta } from './entities/eventos-atleta.entity';

@Injectable()
export class EventosAtletasService {
    constructor(
        @InjectRepository(EventosAtleta)
        private readonly eventoAtletaRepository: Repository<EventosAtleta>,
        @InjectRepository(Evento)
        private readonly eventoRepository: Repository<Evento>,
    ) {}

    async create(request: CreateEventosAtletaDto) {
        const evento = await this.eventoAtletaRepository.create(request);
        return await this.eventoAtletaRepository.save(evento);
    }

    async findAll() {
        return await this.eventoAtletaRepository.find();
    }

    async findOne(id: string) {
        const subQuery = () => {
            const atleta = this.eventoAtletaRepository
                .createQueryBuilder('i')
                .select('i.evento_id')
                .where('atleta_id = :id');
            return atleta.getQuery();
        };
        const evento = await this.eventoRepository
            .createQueryBuilder('j')
            .where('j._id IN (' + subQuery() + ')', { id });
        return await evento.getMany();
    }

    async update(id: string, request: UpdateEventosAtletaDto) {
        const upEvento = await this.eventoAtletaRepository.findOne({ where: { _id: id } });
        upEvento.atleta = request.atleta;
        upEvento.evento = request.evento;
        this.eventoAtletaRepository.merge(upEvento, request);
        return await this.eventoAtletaRepository.save(upEvento);
    }

    async remove(id: string) {
        return await this.eventoAtletaRepository.delete(id);
    }
}
