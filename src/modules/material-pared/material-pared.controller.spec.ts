import { Test, TestingModule } from '@nestjs/testing';
import { MaterialParedController } from './material-pared.controller';
import { MaterialParedService } from './material-pared.service';

describe('MaterialParedController', () => {
    let controller: MaterialParedController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [MaterialParedController],
            providers: [MaterialParedService],
        }).compile();

        controller = module.get<MaterialParedController>(MaterialParedController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
