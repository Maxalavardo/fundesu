import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImgInstalacioneDto } from './dto/create-img-instalacione.dto';
import { UpdateImgInstalacioneDto } from './dto/update-img-instalacione.dto';
import { ImgInstalacione } from './entities/img-instalacione.entity';

@Injectable()
export class ImgInstalacionesService {
    constructor(
        @InjectRepository(ImgInstalacione)
        private readonly imgInstalacioneRepository: Repository<ImgInstalacione>,
    ) {}

    async create(request: CreateImgInstalacioneDto) {
        const img = await this.imgInstalacioneRepository.create(request);
        return await this.imgInstalacioneRepository.save(img);
    }

    async findAll() {
        return await this.imgInstalacioneRepository.find();
    }

    async findOne(id: string) {
        return await this.imgInstalacioneRepository.findOne({
            where: {
                instalaciones: { _id: id },
            },
        });
    }

    async update(id: string, request: UpdateImgInstalacioneDto) {
        const upImg = await this.imgInstalacioneRepository.findOne({ where: { _id: id } });
        upImg.instalaciones = request.instalaciones;
        await this.imgInstalacioneRepository.merge(upImg, request);
        return await this.imgInstalacioneRepository.save(upImg);
    }

    async remove(id: string) {
        return await this.imgInstalacioneRepository.delete(id);
    }
}
