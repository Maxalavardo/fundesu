import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { DisciplinasDeportiva } from 'src/modules/disciplinas-deportivas/entities/disciplinas-deportiva.entity';
import { EventosAtleta } from 'src/modules/eventos-atletas/entities/eventos-atleta.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class DisciplinasAtleta {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    categoria: string;

    @Column({ type: 'varchar' })
    division: string;

    @Column({ type: 'boolean' })
    estatus: boolean;

    @ManyToOne(() => DisciplinasDeportiva, (disciplina) => disciplina.disciplinadAtleta, { eager: true })
    disciplina: DisciplinasDeportiva;

    @ManyToOne(() => Atleta, (atleta) => atleta.disciplinadAtleta, { eager: true })
    atleta: Atleta;
}
