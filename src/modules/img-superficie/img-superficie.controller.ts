import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ImgSuperficieService } from './img-superficie.service';
import { CreateImgSuperficieDto } from './dto/create-img-superficie.dto';
import { UpdateImgSuperficieDto } from './dto/update-img-superficie.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('img-superficie')
@ApiTags('Img-superficie')
export class ImgSuperficieController {
    constructor(private readonly imgSuperficieService: ImgSuperficieService) {}

    @Post()
    @ApiBody({ type: [CreateImgSuperficieDto] })
    create(@Res() Res, @Body() createImgSuperficieDto: CreateImgSuperficieDto) {
        this.imgSuperficieService.create(createImgSuperficieDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Get()
    findAll() {
        return this.imgSuperficieService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.imgSuperficieService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImgSuperficieDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateImgSuperficieDto: UpdateImgSuperficieDto) {
        this.imgSuperficieService.update(id, updateImgSuperficieDto).then((img) => {
            return Res.status(HttpStatus.ACCEPTED).json(img);
        });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.imgSuperficieService.remove(id);
    }
}
