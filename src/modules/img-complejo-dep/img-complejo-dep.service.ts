import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImgComplejoDepDto } from './dto/create-img-complejo-dep.dto';
import { UpdateImgComplejoDepDto } from './dto/update-img-complejo-dep.dto';
import { ImgComplejoDep } from './entities/img-complejo-dep.entity';

@Injectable()
export class ImgComplejoDepService {
    constructor(
        @InjectRepository(ImgComplejoDep)
        private readonly imgComplejoRepository: Repository<ImgComplejoDep>,
    ) {}

    async create(request: CreateImgComplejoDepDto) {
        const img = await this.imgComplejoRepository.create(request);
        return await this.imgComplejoRepository.save(img);
    }

    async findAll() {
        return await this.imgComplejoRepository.find();
    }

    async findOne(id: string) {
        return await this.imgComplejoRepository.findOne({
            where: {
                complejo: {
                    _id: id,
                },
            },
        });
    }

    async update(id: string, request: UpdateImgComplejoDepDto) {
        const upImg = await this.imgComplejoRepository.findOne({ where: { _id: id } });
        upImg.complejo = request.complejo;
        await this.imgComplejoRepository.merge(upImg, request);
        return await this.imgComplejoRepository.save(upImg);
    }

    async remove(id: string) {
        return await this.imgComplejoRepository.delete(id);
    }
}
