import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTallaAtletaDto } from './dto/create-talla-atleta.dto';
import { UpdateTallaAtletaDto } from './dto/update-talla-atleta.dto';
import { TallaAtleta } from './entities/talla-atleta.entity';

@Injectable()
export class TallaAtletaService {
    constructor(
        @InjectRepository(TallaAtleta)
        private readonly tallaAtletaRepository: Repository<TallaAtleta>,
    ) {}

    async create(request: CreateTallaAtletaDto) {
        const atleta = await this.tallaAtletaRepository.create(request);
        return await this.tallaAtletaRepository.save(atleta);
    }

    async findAll() {
        return await this.tallaAtletaRepository.find();
    }

    async findOne(id: string) {
        const complemento = this.tallaAtletaRepository
            .createQueryBuilder('i')
            .leftJoinAndSelect('i.atleta', 'atleta')
            .where('atleta._id = :id', { id })
            .orderBy('i.createdAt', 'DESC');

        return await complemento.getMany();
    }

    async update(id: string, request: UpdateTallaAtletaDto) {
        const upAtleta = await this.tallaAtletaRepository.findOne({ where: { _id: id } });
        await this.tallaAtletaRepository.merge(upAtleta, request);
        return await this.tallaAtletaRepository.save(upAtleta);
    }

    async remove(id: string) {
        return await this.tallaAtletaRepository.delete(id);
    }
}
