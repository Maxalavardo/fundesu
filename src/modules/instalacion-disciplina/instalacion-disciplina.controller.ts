import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { InstalacionDisciplinaService } from './instalacion-disciplina.service';
import { CreateInstalacionDisciplinaDto } from './dto/create-instalacion-disciplina.dto';
import { UpdateInstalacionDisciplinaDto } from './dto/update-instalacion-disciplina.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('instalacion-disciplina')
@ApiTags('Instalacion-disciplina')
export class InstalacionDisciplinaController {
    constructor(private readonly instalacionDisciplinaService: InstalacionDisciplinaService) {}

    @Post()
    @ApiBody({ type: [CreateInstalacionDisciplinaDto] })
    create(@Res() Res, @Body() createInstalacionDisciplinaDto: CreateInstalacionDisciplinaDto) {
        this.instalacionDisciplinaService
            .create(createInstalacionDisciplinaDto)
            .then((instalacionDisc) => {
                return Res.status(HttpStatus.CREATED).json(instalacionDisc);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.instalacionDisciplinaService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.instalacionDisciplinaService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateInstalacionDisciplinaDto] })
    update(
        @Res() Res,
        @Param('id') id: string,
        @Body() updateInstalacionDisciplinaDto: UpdateInstalacionDisciplinaDto,
    ) {
        this.instalacionDisciplinaService
            .update(id, updateInstalacionDisciplinaDto)
            .then((instalacionDisc) => {
                return Res.status(HttpStatus.ACCEPTED).json(instalacionDisc);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.instalacionDisciplinaService.remove(id);
    }
}
