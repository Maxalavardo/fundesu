import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { MaterialPared } from 'src/modules/material-pared/entities/material-pared.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ParedInstalacion {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombrePared: string;

    @Column({ type: 'float' })
    medidaPared: number;

    @ManyToOne(() => InstalacionesDeportiva, (instalaciones) => instalaciones.pared, { eager: true })
    instalaciones: InstalacionesDeportiva;

    @OneToMany(() => MaterialPared, (paredMaterial) => paredMaterial.pared)
    paredMaterial: MaterialPared;
}
