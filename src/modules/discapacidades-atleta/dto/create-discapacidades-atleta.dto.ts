import { IsUUID } from 'class-validator';
import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { Discapacidade } from 'src/modules/discapacidades/entities/discapacidade.entity';

export class CreateDiscapacidadesAtletaDto {
    _id?: string;

    @IsUUID()
    discapacidad: Discapacidade;

    @IsUUID()
    atleta: Atleta;
}
