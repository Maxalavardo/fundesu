import { Module } from '@nestjs/common';
import { EquipamientoDepotivoEspService } from './equipamiento-depotivo-esp.service';
import { EquipamientoDepotivoEspController } from './equipamiento-depotivo-esp.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EquipamientoDepotivoEsp } from './entities/equipamiento-depotivo-esp.entity';

@Module({
    imports: [TypeOrmModule.forFeature([EquipamientoDepotivoEsp])],
    controllers: [EquipamientoDepotivoEspController],
    providers: [EquipamientoDepotivoEspService],
})
export class EquipamientoDepotivoEspModule {}
