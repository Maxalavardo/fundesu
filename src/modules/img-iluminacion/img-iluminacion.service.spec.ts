import { Test, TestingModule } from '@nestjs/testing';
import { ImgIluminacionService } from './img-iluminacion.service';

describe('ImgIluminacionService', () => {
    let service: ImgIluminacionService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ImgIluminacionService],
        }).compile();

        service = module.get<ImgIluminacionService>(ImgIluminacionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
