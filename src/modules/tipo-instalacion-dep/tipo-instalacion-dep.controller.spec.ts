import { Test, TestingModule } from '@nestjs/testing';
import { TipoInstalacionDepController } from './tipo-instalacion-dep.controller';
import { TipoInstalacionDepService } from './tipo-instalacion-dep.service';

describe('TipoInstalacionDepController', () => {
    let controller: TipoInstalacionDepController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [TipoInstalacionDepController],
            providers: [TipoInstalacionDepService],
        }).compile();

        controller = module.get<TipoInstalacionDepController>(TipoInstalacionDepController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
