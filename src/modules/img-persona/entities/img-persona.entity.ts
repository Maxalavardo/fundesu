import { Persona } from 'src/modules/personas/entities/persona.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ImgPersona {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'text', nullable: true })
    image: string;

    @ManyToOne(() => Persona, (persona) => persona.imagePersona, {
        eager: true,
    })
    persona: Persona;
}
