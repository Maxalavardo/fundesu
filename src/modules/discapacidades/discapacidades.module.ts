import { Module } from '@nestjs/common';
import { DiscapacidadesService } from './discapacidades.service';
import { DiscapacidadesController } from './discapacidades.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Discapacidade } from './entities/discapacidade.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Discapacidade])],
    controllers: [DiscapacidadesController],
    providers: [DiscapacidadesService],
})
export class DiscapacidadesModule {}
