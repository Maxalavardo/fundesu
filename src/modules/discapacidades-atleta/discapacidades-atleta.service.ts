import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Discapacidade } from '../discapacidades/entities/discapacidade.entity';
import { CreateDiscapacidadesAtletaDto } from './dto/create-discapacidades-atleta.dto';
import { UpdateDiscapacidadesAtletaDto } from './dto/update-discapacidades-atleta.dto';
import { DiscapacidadesAtleta } from './entities/discapacidades-atleta.entity';

@Injectable()
export class DiscapacidadesAtletaService {
    constructor(
        @InjectRepository(DiscapacidadesAtleta)
        private readonly discapacidadAtletaRepository: Repository<DiscapacidadesAtleta>,
        @InjectRepository(Discapacidade)
        private readonly discapacidadRepository: Repository<Discapacidade>,
    ) {}

    async create(request: CreateDiscapacidadesAtletaDto) {
        const discapacidad = this.discapacidadAtletaRepository.create(request);
        return await this.discapacidadAtletaRepository.save(discapacidad);
    }

    async findAll() {
        return await this.discapacidadAtletaRepository.find();
    }

    async findOne(id: string) {
        const subQuery = () => {
            const atleta = this.discapacidadAtletaRepository
                .createQueryBuilder('i')
                .select('i.discapacidad_id')
                .where('atleta_id = :id');
            return atleta.getQuery();
        };
        const discapacidad = await this.discapacidadRepository
            .createQueryBuilder('j')
            .where('j._id IN (' + subQuery() + ')', { id });
        return await discapacidad.getMany();
    }

    async update(id: string, request: UpdateDiscapacidadesAtletaDto) {
        const upDiscapacidad = await this.discapacidadAtletaRepository.findOne({ where: { _id: id } });
        upDiscapacidad.atleta = request.atleta;
        upDiscapacidad.discapacidad = request.discapacidad;
        this.discapacidadAtletaRepository.merge(upDiscapacidad, request);
        return await this.discapacidadAtletaRepository.save(upDiscapacidad);
    }

    async remove(id: string) {
        return await this.discapacidadAtletaRepository.delete(id);
    }
}
