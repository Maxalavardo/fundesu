import { PartialType } from '@nestjs/mapped-types';
import { CreateImgClubDto } from './create-img-club.dto';

export class UpdateImgClubDto extends PartialType(CreateImgClubDto) {}
