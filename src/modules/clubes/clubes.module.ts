import { Module } from '@nestjs/common';
import { ClubesService } from './clubes.service';
import { ClubesController } from './clubes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Clube } from './entities/clube.entity';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Clube, Notification, Usuario])],
    controllers: [ClubesController],
    providers: [ClubesService],
})
export class ClubesModule {}
