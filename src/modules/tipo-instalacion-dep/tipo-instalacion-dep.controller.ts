import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Request } from '@nestjs/common';
import { TipoInstalacionDepService } from './tipo-instalacion-dep.service';
import { CreateTipoInstalacionDepDto } from './dto/create-tipo-instalacion-dep.dto';
import { UpdateTipoInstalacionDepDto } from './dto/update-tipo-instalacion-dep.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';

@Controller('tipo-instalacion-dep')
@ApiTags('Tipo-instalacion-deportiva')
export class TipoInstalacionDepController {
    constructor(private readonly tipoInstalacionDepService: TipoInstalacionDepService) {}

    @AuthAll()
    @Post()
    @ApiBody({ type: [CreateTipoInstalacionDepDto] })
    create(@Request() req, @Res() Res, @Body() createTipoInstalacionDepDto: CreateTipoInstalacionDepDto) {
        this.tipoInstalacionDepService
            .create(createTipoInstalacionDepDto, req.user)
            .then((instalacion) => {
                return Res.status(HttpStatus.CREATED).json(instalacion);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.tipoInstalacionDepService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.tipoInstalacionDepService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateTipoInstalacionDepDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateTipoInstalacionDepDto: UpdateTipoInstalacionDepDto) {
        this.tipoInstalacionDepService
            .update(id, updateTipoInstalacionDepDto)
            .then((instalacion) => {
                return Res.status(HttpStatus.CREATED).json(instalacion);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.tipoInstalacionDepService.remove(id, req.user);
    }
}
