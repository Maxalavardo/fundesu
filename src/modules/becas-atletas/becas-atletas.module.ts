import { Module } from '@nestjs/common';
import { BecasAtletasService } from './becas-atletas.service';
import { BecasAtletasController } from './becas-atletas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BecasAtleta } from './entities/becas-atleta.entity';
import { Beca } from '../becas/entities/beca.entity';

@Module({
    imports: [TypeOrmModule.forFeature([BecasAtleta, Beca])],
    controllers: [BecasAtletasController],
    providers: [BecasAtletasService],
})
export class BecasAtletasModule {}
