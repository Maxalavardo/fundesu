import { Module } from '@nestjs/common';
import { ComplementosInstalacionService } from './complementos-instalacion.service';
import { ComplementosInstalacionController } from './complementos-instalacion.controller';
import { ComplementosInstalacion } from './entities/complementos-instalacion.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
    imports: [TypeOrmModule.forFeature([ComplementosInstalacion])],
    controllers: [ComplementosInstalacionController],
    providers: [ComplementosInstalacionService],
})
export class ComplementosInstalacionModule {}
