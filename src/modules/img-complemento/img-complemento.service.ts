import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImgComplementoDto } from './dto/create-img-complemento.dto';
import { UpdateImgComplementoDto } from './dto/update-img-complemento.dto';
import { ImgComplemento } from './entities/img-complemento.entity';

@Injectable()
export class ImgComplementoService {
    constructor(
        @InjectRepository(ImgComplemento)
        private readonly imgComplementoRepository: Repository<ImgComplemento>,
    ) {}

    async create(request: CreateImgComplementoDto) {
        const img = await this.imgComplementoRepository.create(request);
        return await this.imgComplementoRepository.save(img);
    }

    async findAll() {
        return await this.imgComplementoRepository.find();
    }

    async findOne(id: string) {
        return await this.imgComplementoRepository.findOne({
            where: {
                instalaciones: { _id: id },
            },
        });
    }

    async update(id: string, request: UpdateImgComplementoDto) {
        const upImg = await this.imgComplementoRepository.findOne({ where: { _id: id } });
        upImg.instalaciones = request.instalaciones;
        await this.imgComplementoRepository.merge(upImg, request);
        return await this.imgComplementoRepository.save(upImg);
    }

    async remove(id: string) {
        return await this.imgComplementoRepository.delete(id);
    }
}
