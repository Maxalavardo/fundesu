import { PartialType } from '@nestjs/mapped-types';
import { CreateMaterialIluminacionDto } from './create-material-iluminacion.dto';

export class UpdateMaterialIluminacionDto extends PartialType(CreateMaterialIluminacionDto) {}
