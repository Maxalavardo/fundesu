import { IsUUID } from 'class-validator';
import { DisciplinasDeportiva } from 'src/modules/disciplinas-deportivas/entities/disciplinas-deportiva.entity';
import { TipoInstalacionDep } from 'src/modules/tipo-instalacion-dep/entities/tipo-instalacion-dep.entity';

export class CreateInstalacionDisciplinaDto {
    _id?: string;

    @IsUUID()
    tipoInstalacion: TipoInstalacionDep;

    @IsUUID()
    disciplina: DisciplinasDeportiva;
}
