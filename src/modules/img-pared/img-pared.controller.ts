import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ImgParedService } from './img-pared.service';
import { CreateImgParedDto } from './dto/create-img-pared.dto';
import { UpdateImgParedDto } from './dto/update-img-pared.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('img-pared')
@ApiTags('Img-pared')
export class ImgParedController {
    constructor(private readonly imgParedService: ImgParedService) {}

    @Post()
    @ApiBody({ type: [CreateImgParedDto] })
    create(@Res() Res, @Body() createImgParedDto: CreateImgParedDto) {
        this.imgParedService.create(createImgParedDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Get()
    findAll() {
        return this.imgParedService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.imgParedService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImgParedDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateImgParedDto: UpdateImgParedDto) {
        this.imgParedService.update(id, updateImgParedDto).then((img) => {
            return Res.status(HttpStatus.ACCEPTED).json(img);
        });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.imgParedService.remove(id);
    }
}
