import { PartialType } from '@nestjs/mapped-types';
import { CreateComplementosInstalacionDto } from './create-complementos-instalacion.dto';

export class UpdateComplementosInstalacionDto extends PartialType(CreateComplementosInstalacionDto) {}
