import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateComunidadeDto } from './dto/create-comunidade.dto';
import { UpdateComunidadeDto } from './dto/update-comunidade.dto';
import { Comunidades } from './entities/comunidade.entity';

@Injectable()
export class ComunidadesService {
    constructor(
        @InjectRepository(Comunidades)
        private readonly comunidadRepository: Repository<Comunidades>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    async create(request: CreateComunidadeDto, user: any) {
        let comunidad = this.comunidadRepository.create(request);
        comunidad = await this.comunidadRepository.save(comunidad);

        const message = 'Una comunidad a sido registrada';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Comunidades.name,
            uuid: comunidad._id,
            data: comunidad,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return comunidad;
    }

    async findAll(parroquiaId?: string) {
        const comunidad = this.comunidadRepository
            .createQueryBuilder('i')
            .innerJoinAndSelect('i.parroquia', 'parroquia')
            .innerJoinAndSelect('parroquia.municipio', 'municipio')
            .innerJoinAndSelect('municipio.estado', 'estado')
            .where('1 = 1');

        if (parroquiaId) {
            comunidad.andWhere('parroquia._id = :parroquiaId', { parroquiaId });
        }

        return await comunidad.getMany();
    }

    async findOne(id: string) {
        return await this.comunidadRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateComunidadeDto) {
        const upComunidad = await this.comunidadRepository.findOne({ where: { _id: id } });
        upComunidad.parroquia = request.parroquia;
        this.comunidadRepository.merge(upComunidad, request);
        return await this.comunidadRepository.save(upComunidad);
    }

    async remove(id: string, user: any) {
        const comunidad = await this.comunidadRepository.findOne({ where: { _id: id } });

        const message = 'Una comunidad a sido eliminada';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Comunidades.name,
            uuid: id,
            data: comunidad,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.comunidadRepository.softDelete(id);
    }
}
