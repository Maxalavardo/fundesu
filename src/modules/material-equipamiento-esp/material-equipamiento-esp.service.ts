import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Materiale } from '../materiales/entities/materiale.entity';
import { CreateMaterialEquipamientoEspDto } from './dto/create-material-equipamiento-esp.dto';
import { UpdateMaterialEquipamientoEspDto } from './dto/update-material-equipamiento-esp.dto';
import { MaterialEquipamientoEsp } from './entities/material-equipamiento-esp.entity';

@Injectable()
export class MaterialEquipamientoEspService {
    constructor(
        @InjectRepository(MaterialEquipamientoEsp)
        private readonly materialEquiRepository: Repository<MaterialEquipamientoEsp>,
        @InjectRepository(Materiale)
        private readonly materialRespository: Repository<Materiale>,
    ) {}

    async create(request: CreateMaterialEquipamientoEspDto) {
        const equipamiento = await this.materialEquiRepository.create(request);
        return await this.materialEquiRepository.save(equipamiento);
    }

    async findAll() {
        return await this.materialEquiRepository.find();
    }

    async findOne(id: string) {
        const material = await this.materialEquiRepository.find({
            relations: [],
            where: {
                equipamiento: {
                    _id: id,
                },
            },
        });
        return material;
    }

    async update(id: string, request: UpdateMaterialEquipamientoEspDto) {
        const upEquipamiento = await this.materialEquiRepository.findOne({ where: { _id: id } });
        upEquipamiento.equipamiento = request.equipamiento;
        upEquipamiento.material = request.material;
        await this.materialEquiRepository.merge(upEquipamiento, request);
        return await this.materialEquiRepository.save(upEquipamiento);
    }

    async remove(id: string) {
        return await this.materialEquiRepository.delete(id);
    }
}
