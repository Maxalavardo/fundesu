import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { DisciplinasDeportiva } from '../disciplinas-deportivas/entities/disciplinas-deportiva.entity';
import { CreateDisciplinasAtletaDto } from './dto/create-disciplinas-atleta.dto';
import { UpdateDisciplinasAtletaDto } from './dto/update-disciplinas-atleta.dto';
import { DisciplinasAtleta } from './entities/disciplinas-atleta.entity';

@Injectable()
export class DisciplinasAtletaService {
    constructor(
        @InjectRepository(DisciplinasAtleta)
        private readonly disciplinaAtletaRepository: Repository<DisciplinasAtleta>,
        @InjectRepository(DisciplinasDeportiva)
        private readonly disciplinaRepository: Repository<DisciplinasDeportiva>,
    ) {}

    async create(request: CreateDisciplinasAtletaDto) {
        const disciplina = this.disciplinaAtletaRepository.create(request);
        return await this.disciplinaAtletaRepository.save(disciplina);
    }

    async findAll() {
        return await this.disciplinaAtletaRepository.find();
    }

    async findOne(id: string) {
        const subQuery = () => {
            const atleta = this.disciplinaAtletaRepository
                .createQueryBuilder('i')
                .select('i.disciplina_id')
                .where('atleta_id = :id');
            return atleta.getQuery();
        };
        const disciplina = await this.disciplinaRepository
            .createQueryBuilder('j')
            .where('j._id IN (' + subQuery() + ')', { id });
        return await disciplina.getMany();
    }

    async update(id: string, request: UpdateDisciplinasAtletaDto) {
        const upDisciplina = await this.disciplinaAtletaRepository.findOne({ where: { _id: id } });
        upDisciplina.atleta = request.atleta;
        upDisciplina.disciplina = request.disciplina;
        this.disciplinaAtletaRepository.merge(upDisciplina, request);
        return await this.disciplinaAtletaRepository.save(upDisciplina);
    }

    async remove(id: string) {
        return await this.disciplinaAtletaRepository.delete(id);
    }
}
