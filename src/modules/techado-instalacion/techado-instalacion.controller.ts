import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Res } from '@nestjs/common';
import { TechadoInstalacionService } from './techado-instalacion.service';
import { CreateTechadoInstalacionDto } from './dto/create-techado-instalacion.dto';
import { UpdateTechadoInstalacionDto } from './dto/update-techado-instalacion.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('techado-instalacion')
@ApiTags('Techado-instalacion')
export class TechadoInstalacionController {
    constructor(private readonly techadoInstalacionService: TechadoInstalacionService) {}

    @Post()
    @ApiBody({ type: [CreateTechadoInstalacionDto] })
    create(@Res() Res, @Body() createTechadoInstalacionDto: CreateTechadoInstalacionDto) {
        this.techadoInstalacionService
            .create(createTechadoInstalacionDto)
            .then((techado) => {
                return Res.status(HttpStatus.CREATED).json(techado);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.techadoInstalacionService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.techadoInstalacionService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateTechadoInstalacionDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateTechadoInstalacionDto: UpdateTechadoInstalacionDto) {
        this.techadoInstalacionService
            .update(id, updateTechadoInstalacionDto)
            .then((techado) => {
                return Res.status(HttpStatus.ACCEPTED).json(techado);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.techadoInstalacionService.remove(id);
    }
}
