import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Res } from '@nestjs/common';
import { ImplementosDeportivosService } from './implementos-deportivos.service';
import { CreateImplementosDeportivoDto } from './dto/create-implementos-deportivo.dto';
import { UpdateImplementosDeportivoDto } from './dto/update-implementos-deportivo.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('implementos-deportivos')
@ApiTags('Implementos-deportivos')
export class ImplementosDeportivosController {
    constructor(private readonly implementosDeportivosService: ImplementosDeportivosService) {}

    @Post()
    @ApiBody({ type: [CreateImplementosDeportivoDto] })
    create(@Res() Res, @Body() createImplementosDeportivoDto: CreateImplementosDeportivoDto) {
        this.implementosDeportivosService
            .create(createImplementosDeportivoDto)
            .then((implementos) => {
                return Res.status(HttpStatus.CREATED).json(implementos);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.implementosDeportivosService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.implementosDeportivosService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImplementosDeportivoDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateImplementosDeportivoDto: UpdateImplementosDeportivoDto) {
        this.implementosDeportivosService
            .update(id, updateImplementosDeportivoDto)
            .then((implementos) => {
                return Res.status(HttpStatus.CREATED).json(implementos);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.implementosDeportivosService.remove(id);
    }
}
