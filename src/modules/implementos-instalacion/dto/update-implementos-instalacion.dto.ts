import { PartialType } from '@nestjs/mapped-types';
import { CreateImplementosInstalacionDto } from './create-implementos-instalacion.dto';

export class UpdateImplementosInstalacionDto extends PartialType(CreateImplementosInstalacionDto) {}
