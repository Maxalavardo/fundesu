import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus, Request } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthAll } from '../auth/decorator/auth.decorator';
import { ComplejosDeportivosService } from './complejos-deportivos.service';
import { CreateComplejosDeportivoDto } from './dto/create-complejos-deportivo.dto';
import { UpdateComplejosDeportivoDto } from './dto/update-complejos-deportivo.dto';

@Controller('complejos-deportivos')
@ApiTags('Complejos-deportivos')
export class ComplejosDeportivosController {
    constructor(private readonly complejosDeportivosService: ComplejosDeportivosService) {}

    @AuthAll()
    @Post('/')
    @ApiBody({ type: [CreateComplejosDeportivoDto] })
    create(@Request() req, @Res() Res, @Body() createComplejosDeportivoDto: CreateComplejosDeportivoDto) {
        this.complejosDeportivosService
            .create(createComplejosDeportivoDto, req.user)
            .then((complejo) => {
                return Res.status(HttpStatus.CREATED).json(complejo);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @Get()
    findAll() {
        return this.complejosDeportivosService.findAll();
    }

    @Get('/total-complejos')
    countAll() {
        return this.complejosDeportivosService.count();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.complejosDeportivosService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateComplejosDeportivoDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateComplejosDeportivoDto: UpdateComplejosDeportivoDto) {
        this.complejosDeportivosService
            .update(id, updateComplejosDeportivoDto)
            .then((complejo) => {
                return Res.status(HttpStatus.ACCEPTED).json(complejo);
            })
            .catch(() => {
                return Res.status(HttpStatus.NOT_FOUND).json({ message: 'Error' });
            });
    }

    @AuthAll()
    @Delete(':id')
    remove(@Request() req, @Param('id') id: string) {
        return this.complejosDeportivosService.remove(id, req.user);
    }
}
