import { IsInt, IsUUID } from 'class-validator';
import { ImplementosDeportivo } from '../../implementos-deportivos/entities/implementos-deportivo.entity';
import { InstalacionesDeportiva } from '../../instalaciones-deportivas/entities/instalaciones-deportiva.entity';

export class CreateImplementosInstalacionDto {
    _id?: string;

    @IsInt()
    cantidadImplemento: number;

    @IsUUID()
    instalaciones: InstalacionesDeportiva;

    @IsUUID()
    implemento: ImplementosDeportivo;
}
