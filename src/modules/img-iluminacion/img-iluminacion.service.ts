import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImgIluminacionDto } from './dto/create-img-iluminacion.dto';
import { UpdateImgIluminacionDto } from './dto/update-img-iluminacion.dto';
import { ImgIluminacion } from './entities/img-iluminacion.entity';

@Injectable()
export class ImgIluminacionService {
    constructor(
        @InjectRepository(ImgIluminacion)
        private readonly imgIluminacionRepository: Repository<ImgIluminacion>,
    ) {}

    async create(request: CreateImgIluminacionDto) {
        const img = await this.imgIluminacionRepository.create(request);
        return await this.imgIluminacionRepository.save(img);
    }

    async findAll() {
        return await this.imgIluminacionRepository.find();
    }

    async findOne(id: string) {
        return await this.imgIluminacionRepository.findOne({
            where: {
                instalaciones: { _id: id },
            },
        });
    }

    async update(id: string, request: UpdateImgIluminacionDto) {
        const upImg = await this.imgIluminacionRepository.findOne({ where: { _id: id } });
        upImg.instalaciones = request.instalaciones;
        await this.imgIluminacionRepository.merge(upImg, request);
        return await this.imgIluminacionRepository.save(upImg);
    }

    async remove(id: string) {
        return await this.imgIluminacionRepository.delete(id);
    }
}
