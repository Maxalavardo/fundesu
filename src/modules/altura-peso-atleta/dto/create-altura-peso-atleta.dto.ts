import { IsBoolean, IsNumber, IsUUID } from 'class-validator';
import { Atleta } from 'src/modules/atletas/entities/atleta.entity';

export class CreateAlturaPesoAtletaDto {
    _id?: string;

    @IsNumber()
    peso: number;

    @IsNumber()
    estatura: number;

    @IsNumber()
    presionArterial: number;

    @IsBoolean()
    statusAlturaPeso: boolean;

    @IsUUID()
    atleta: Atleta;
}
