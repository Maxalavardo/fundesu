import { Module } from '@nestjs/common';
import { ComplejosClubesService } from './complejos-clubes.service';
import { ComplejosClubesController } from './complejos-clubes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ComplejosClube } from './entities/complejos-clube.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ComplejosClube])],
    controllers: [ComplejosClubesController],
    providers: [ComplejosClubesService],
})
export class ComplejosClubesModule {}
