import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import {
    Column,
    CreateDateColumn,
    DeleteDateColumn,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
} from 'typeorm';

@Entity()
export class TallaAtleta {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    tallaFranela: string;

    @Column({ type: 'varchar' })
    tallaPantalon: string;

    @Column({ type: 'varchar' })
    tallaZapatos: string;

    @Column({ type: 'boolean' })
    statusTalla: boolean;

    @ManyToOne(() => Atleta, (atleta) => atleta.persona)
    atleta: Atleta;

    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

    @Column({ nullable: true })
    @DeleteDateColumn()
    deleteAt?: Date;
}
