import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Materiale } from '../materiales/entities/materiale.entity';
import { CreateMaterialSuperficieDto } from './dto/create-material-superficie.dto';
import { UpdateMaterialSuperficieDto } from './dto/update-material-superficie.dto';
import { MaterialSuperficie } from './entities/material-superficie.entity';

@Injectable()
export class MaterialSuperficieService {
    constructor(
        @InjectRepository(MaterialSuperficie)
        private readonly materialSuperRepository: Repository<MaterialSuperficie>,
        @InjectRepository(Materiale)
        private readonly materialRespository: Repository<Materiale>,
    ) {}

    async create(request: CreateMaterialSuperficieDto) {
        const superficie = await this.materialSuperRepository.create(request);
        return await this.materialSuperRepository.save(superficie);
    }

    async findAll() {
        return await this.materialSuperRepository.find();
    }

    async findOne(id: string) {
        const material = await this.materialSuperRepository.find({
            relations: [],
            where: {
                superficie: {
                    _id: id,
                },
            },
        });
        return material;
    }

    async update(id: string, request: UpdateMaterialSuperficieDto) {
        const upSuperficie = await this.materialSuperRepository.findOne({ where: { _id: id } });
        upSuperficie.superficie = request.superficie;
        upSuperficie.material = request.material;
        await this.materialSuperRepository.merge(upSuperficie, request);
        return await this.materialSuperRepository.save(upSuperficie);
    }

    async remove(id: string) {
        return await this.materialSuperRepository.delete(id);
    }
}
