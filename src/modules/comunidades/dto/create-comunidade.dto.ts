import { IsString, IsUUID } from 'class-validator';
import { Parroquia } from 'src/modules/parroquias/entities/parroquia.entity';

export class CreateComunidadeDto {
    _id?: string;

    @IsString()
    nombreComunidad: string;

    @IsUUID()
    parroquia: Parroquia;
}
