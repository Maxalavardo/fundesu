import { JwtService } from '@nestjs/jwt';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { JWTPayload } from './dto/jwt.payload';
import { IToken } from './interfaces/itoken.interfaces';

export class JWTToken implements IToken {
    private readonly hash: string;
    private readonly user: Usuario;
    private readonly payload: JWTPayload;

    constructor(private readonly jwtService: JwtService, user: Usuario) {
        this.user = user;
        this.payload = {
            _id: user._id,
            usuario: user.usuario,
            createdAt: user.createdAt,
        };

        this.hash = this.jwtService.sign(this.payload);
    }

    getHash(): string {
        return this.hash;
    }

    getPayload(): JWTPayload {
        return this.payload;
    }

    getUser(): any {
        return this.user;
    }
}
