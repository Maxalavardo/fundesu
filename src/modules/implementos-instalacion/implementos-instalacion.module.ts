import { Module } from '@nestjs/common';
import { ImplementosInstalacionService } from './implementos-instalacion.service';
import { ImplementosInstalacionController } from './implementos-instalacion.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImplementosInstalacion } from './entities/implementos-instalacion.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImplementosInstalacion])],
    controllers: [ImplementosInstalacionController],
    providers: [ImplementosInstalacionService],
})
export class ImplementosInstalacionModule {}
