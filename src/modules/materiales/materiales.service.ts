import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateMaterialeDto } from './dto/create-materiale.dto';
import { UpdateMaterialeDto } from './dto/update-materiale.dto';
import { Materiale } from './entities/materiale.entity';

@Injectable()
export class MaterialesService {
    constructor(
        @InjectRepository(Materiale)
        private readonly materialRespository: Repository<Materiale>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    async create(request: CreateMaterialeDto, user: any): Promise<Materiale> {
        let material = this.materialRespository.create(request);
        material = await this.materialRespository.save(material);

        const message = 'Un material a sido registrado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Materiale.name,
            uuid: material._id,
            data: material,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return material;
    }

    async findAll() {
        return await this.materialRespository.find();
    }

    async findOne(id: string) {
        return await this.materialRespository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateMaterialeDto): Promise<Materiale> {
        const upMaterial = await this.materialRespository.findOne({ where: { _id: id } });
        await this.materialRespository.merge(upMaterial, request);
        return await this.materialRespository.save(upMaterial);
    }

    async remove(id: string, user: any) {
        const material = await this.materialRespository.findOne({ where: { _id: id } });

        const message = 'Un material a sido eliminado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Materiale.name,
            uuid: id,
            data: material,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.materialRespository.delete(id);
    }
}
