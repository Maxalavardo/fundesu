import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { Discapacidade } from 'src/modules/discapacidades/entities/discapacidade.entity';
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class DiscapacidadesAtleta {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @ManyToOne(() => Discapacidade, (discapacidad) => discapacidad.discapacidadAtleta, { eager: true })
    discapacidad: Discapacidade;

    @ManyToOne(() => Atleta, (atleta) => atleta.discapacidadAtleta, { eager: true })
    atleta: Atleta;
}
