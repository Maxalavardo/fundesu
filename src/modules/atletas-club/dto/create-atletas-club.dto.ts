import { IsBoolean, IsString, IsUUID } from 'class-validator';
import { Atleta } from 'src/modules/atletas/entities/atleta.entity';
import { Clube } from 'src/modules/clubes/entities/clube.entity';

export class CreateAtletasClubDto {
    _id?: string;

    @IsUUID()
    atleta: Atleta;

    @IsUUID()
    club: Clube;

    @IsString()
    fechaIngreso: Date;

    @IsString()
    fechaSalida?: Date;

    @IsBoolean()
    estatus: boolean;
}
