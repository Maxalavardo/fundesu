import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Materiale } from '../materiales/entities/materiale.entity';
import { CreateMaterialTechadoDto } from './dto/create-material-techado.dto';
import { UpdateMaterialTechadoDto } from './dto/update-material-techado.dto';
import { MaterialTechado } from './entities/material-techado.entity';

@Injectable()
export class MaterialTechadoService {
    constructor(
        @InjectRepository(MaterialTechado)
        private readonly materialTechaRepository: Repository<MaterialTechado>,
        @InjectRepository(Materiale)
        private readonly materialRespository: Repository<Materiale>,
    ) {}

    async create(request: CreateMaterialTechadoDto) {
        const techado = await this.materialTechaRepository.create(request);
        return await this.materialTechaRepository.save(techado);
    }

    async findAll() {
        return await this.materialTechaRepository.find();
    }

    async findOne(id: string) {
        const material = await this.materialTechaRepository.find({
            relations: [],
            where: {
                techado: {
                    _id: id,
                },
            },
        });
        return material;
    }

    async update(id: string, request: UpdateMaterialTechadoDto) {
        const upTechado = await this.materialTechaRepository.findOne({ where: { _id: id } });
        upTechado.techado = request.techado;
        upTechado.material = request.material;
        await this.materialTechaRepository.merge(upTechado, request);
        return await this.materialTechaRepository.save(upTechado);
    }

    async remove(id: string) {
        return await this.materialTechaRepository.delete(id);
    }
}
