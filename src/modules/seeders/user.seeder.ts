import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { Persona } from '../personas/entities/persona.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { UserRoleEnum } from '../usuarios/enums/user-rol.enum';
import { CreatePersonInterface, CreateUserInterface } from './dto/create-userPerson.dto';

@Injectable()
export class UserSeeder implements Seeder {
    constructor(
        @InjectRepository(Usuario)
        private readonly userRepository: Repository<Usuario>,
        @InjectRepository(Persona)
        private readonly personRepository: Repository<Persona>,
    ) {}

    async seed(): Promise<any> {
        try {
            const UserDTO: CreateUserInterface = {
                usuario: 'admin@IDES.com',
                contrasena: '1234567890',
                rol: UserRoleEnum.SUPER_ADMIN,
                isActive: true,
            };

            const PersonDTO: CreatePersonInterface = {
                cedula: '123456789',
                nombres: 'admin',
                apellidos: 'admin',
                telefono: '1234567890',
                tlfcasa: '1234567890',
                sexo: 'Masculino',
                fechanac: new Date(),
                direccion: 'IDES',
            };

            const user: Usuario = this.userRepository.create(UserDTO);

            const person: Persona = this.personRepository.create(PersonDTO);

            user.persona = await this.personRepository.save(person);

            return await this.userRepository.save(user);
        } catch (exception: any) {
            console.log('Crear usuario: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const user = await this.userRepository.findOne({
            where: { usuario: 'admin@IDES.com' },
        });

        return await this.userRepository.remove(user);
    }
}
