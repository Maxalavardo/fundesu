import { Test, TestingModule } from '@nestjs/testing';
import { ComplejosClubesController } from './complejos-clubes.controller';
import { ComplejosClubesService } from './complejos-clubes.service';

describe('ComplejosClubesController', () => {
    let controller: ComplejosClubesController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ComplejosClubesController],
            providers: [ComplejosClubesService],
        }).compile();

        controller = module.get<ComplejosClubesController>(ComplejosClubesController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
