import { Test, TestingModule } from '@nestjs/testing';
import { ComplejosDeportivosService } from './complejos-deportivos.service';

describe('ComplejosDeportivosService', () => {
    let service: ComplejosDeportivosService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ComplejosDeportivosService],
        }).compile();

        service = module.get<ComplejosDeportivosService>(ComplejosDeportivosService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
