import { Module } from '@nestjs/common';
import { AtletasService } from './atletas.service';
import { AtletasController } from './atletas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Atleta } from './entities/atleta.entity';
import { ViewAtleta } from './entities/viewAtleta.entity';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Atleta, ViewAtleta, Notification, Usuario])],
    controllers: [AtletasController],
    providers: [AtletasService],
})
export class AtletasModule {}
