import { PartialType } from '@nestjs/mapped-types';
import { CreateImgPersonaDto } from './create-img-persona.dto';

export class UpdateImgPersonaDto extends PartialType(CreateImgPersonaDto) {}
