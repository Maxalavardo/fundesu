import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { TallaAtletaService } from './talla-atleta.service';
import { CreateTallaAtletaDto } from './dto/create-talla-atleta.dto';
import { UpdateTallaAtletaDto } from './dto/update-talla-atleta.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('talla-atleta')
@ApiTags('Talla-atleta')
export class TallaAtletaController {
    constructor(private readonly tallaAtletaService: TallaAtletaService) {}

    @Post()
    @ApiBody({ type: [CreateTallaAtletaDto] })
    create(@Res() Res, @Body() createTallaAtletaDto: CreateTallaAtletaDto) {
        this.tallaAtletaService
            .create(createTallaAtletaDto)
            .then((tallaAtleta) => {
                return Res.status(HttpStatus.CREATED).json(tallaAtleta);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.tallaAtletaService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.tallaAtletaService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateTallaAtletaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateTallaAtletaDto: UpdateTallaAtletaDto) {
        this.tallaAtletaService
            .update(id, updateTallaAtletaDto)
            .then((tallaAtleta) => {
                return Res.status(HttpStatus.ACCEPTED).json(tallaAtleta);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.tallaAtletaService.remove(id);
    }
}
