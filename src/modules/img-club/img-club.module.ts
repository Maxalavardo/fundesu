import { Module } from '@nestjs/common';
import { ImgClubService } from './img-club.service';
import { ImgClubController } from './img-club.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImgClub } from './entities/img-club.entity';
import { Clube } from '../clubes/entities/clube.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImgClub, Clube])],
    controllers: [ImgClubController],
    providers: [ImgClubService],
})
export class ImgClubModule {}
