import { Module } from '@nestjs/common';
import { EquipamientoInstalacionService } from './equipamiento-instalacion.service';
import { EquipamientoInstalacionController } from './equipamiento-instalacion.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EquipamientoInstalacion } from './entities/equipamiento-instalacion.entity';

@Module({
    imports: [TypeOrmModule.forFeature([EquipamientoInstalacion])],
    controllers: [EquipamientoInstalacionController],
    providers: [EquipamientoInstalacionService],
})
export class EquipamientoInstalacionModule {}
