import { Test, TestingModule } from '@nestjs/testing';
import { EventosAtletasService } from './eventos-atletas.service';

describe('EventosAtletasService', () => {
    let service: EventosAtletasService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [EventosAtletasService],
        }).compile();

        service = module.get<EventosAtletasService>(EventosAtletasService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
