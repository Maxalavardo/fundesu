import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ImgInstalacione {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'text', nullable: true })
    image: string;

    @ManyToOne(() => InstalacionesDeportiva, (instalaciones) => instalaciones.imageInstalaciones, { eager: true })
    instalaciones: InstalacionesDeportiva;
}
