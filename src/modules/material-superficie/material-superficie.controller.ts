import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { MaterialSuperficieService } from './material-superficie.service';
import { CreateMaterialSuperficieDto } from './dto/create-material-superficie.dto';
import { UpdateMaterialSuperficieDto } from './dto/update-material-superficie.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('material-superficie')
@ApiTags('Material-superficie')
export class MaterialSuperficieController {
    constructor(private readonly materialSuperficieService: MaterialSuperficieService) {}

    @Post()
    @ApiBody({ type: [CreateMaterialSuperficieDto] })
    create(@Res() Res, @Body() createMaterialSuperficieDto: CreateMaterialSuperficieDto) {
        this.materialSuperficieService
            .create(createMaterialSuperficieDto)
            .then((superficie) => {
                return Res.status(HttpStatus.CREATED).json(superficie);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.materialSuperficieService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.materialSuperficieService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateMaterialSuperficieDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateMaterialSuperficieDto: UpdateMaterialSuperficieDto) {
        this.materialSuperficieService
            .update(id, updateMaterialSuperficieDto)
            .then((superficie) => {
                return Res.status(HttpStatus.ACCEPTED).json(superficie);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.materialSuperficieService.remove(id);
    }
}
