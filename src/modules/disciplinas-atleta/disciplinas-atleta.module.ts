import { Module } from '@nestjs/common';
import { DisciplinasAtletaService } from './disciplinas-atleta.service';
import { DisciplinasAtletaController } from './disciplinas-atleta.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DisciplinasAtleta } from './entities/disciplinas-atleta.entity';
import { DisciplinasDeportiva } from '../disciplinas-deportivas/entities/disciplinas-deportiva.entity';

@Module({
    imports: [TypeOrmModule.forFeature([DisciplinasAtleta, DisciplinasDeportiva])],
    controllers: [DisciplinasAtletaController],
    providers: [DisciplinasAtletaService],
})
export class DisciplinasAtletaModule {}
