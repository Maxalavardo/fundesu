import { ImplementosInstalacion } from 'src/modules/implementos-instalacion/entities/implementos-instalacion.entity';
import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ImplementosDeportivo {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreImplemento: string;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => ImplementosInstalacion, (implementosIns) => implementosIns.implemento)
    implementosIsn: ImplementosInstalacion;
}
