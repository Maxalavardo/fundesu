import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMunicipioDto } from './dto/create-municipio.dto';
import { UpdateMunicipioDto } from './dto/update-municipio.dto';
import { Municipio } from './entities/municipio.entity';

@Injectable()
export class MunicipiosService {
    constructor(
        @InjectRepository(Municipio)
        private readonly municipiorepository: Repository<Municipio>,
    ) {}

    create(request: CreateMunicipioDto): Promise<Municipio> {
        const municipio = this.municipiorepository.create(request);
        return this.municipiorepository.save(municipio);
    }

    async findAll(estadoId?: string): Promise<Municipio[]> {
        const municipios = this.municipiorepository
            .createQueryBuilder('i')
            .innerJoinAndSelect('i.estado', 'estado')
            .where('1 = 1');

        if (estadoId) {
            municipios.andWhere('estado._id = :estadoId', { estadoId });
        }

        return await municipios.getMany();
    }

    async findOne(_id: string): Promise<Municipio> {
        return await this.municipiorepository.findOne({ where: { _id: _id } });
    }

    async update(id: string, request: UpdateMunicipioDto) {
        const upMunicipio = await this.municipiorepository.findOne({ where: { _id: id } });
        upMunicipio.estado = request.estado;
        await this.municipiorepository.merge(upMunicipio, request);
        return await this.municipiorepository.save(upMunicipio);
    }

    async remove(id: string) {
        return await this.municipiorepository.softDelete(id);
    }
}
