import { PartialType } from '@nestjs/mapped-types';
import { CreateMaterialEquipamientoEspDto } from './create-material-equipamiento-esp.dto';

export class UpdateMaterialEquipamientoEspDto extends PartialType(CreateMaterialEquipamientoEspDto) {}
