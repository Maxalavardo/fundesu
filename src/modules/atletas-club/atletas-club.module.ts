import { Module } from '@nestjs/common';
import { AtletasClubService } from './atletas-club.service';
import { AtletasClubController } from './atletas-club.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AtletasClub } from './entities/atletas-club.entity';

@Module({
    imports: [TypeOrmModule.forFeature([AtletasClub])],
    controllers: [AtletasClubController],
    providers: [AtletasClubService],
})
export class AtletasClubModule {}
