import { Module } from '@nestjs/common';
import { MaterialIluminacionService } from './material-iluminacion.service';
import { MaterialIluminacionController } from './material-iluminacion.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MaterialIluminacion } from './entities/material-iluminacion.entity';
import { Materiale } from '../materiales/entities/materiale.entity';

@Module({
    imports: [TypeOrmModule.forFeature([MaterialIluminacion, Materiale])],
    controllers: [MaterialIluminacionController],
    providers: [MaterialIluminacionService],
})
export class MaterialIluminacionModule {}
