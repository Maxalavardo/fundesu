import { ImgTechado } from 'src/modules/img-techado/entities/img-techado.entity';
import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { MaterialTechado } from 'src/modules/material-techado/entities/material-techado.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TechadoInstalacion {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreTechado: string;

    @Column({ type: 'float' })
    medidaTechado: number;

    @ManyToOne(() => InstalacionesDeportiva, (instalaciones) => instalaciones.techado, { eager: true })
    instalaciones: InstalacionesDeportiva;

    @OneToMany(() => MaterialTechado, (techadoMaterial) => techadoMaterial.techado)
    techadoMaterial: MaterialTechado;
}
