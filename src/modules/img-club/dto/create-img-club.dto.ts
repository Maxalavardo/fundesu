import { IsString, IsUUID } from 'class-validator';
import { Clube } from 'src/modules/clubes/entities/clube.entity';

export class CreateImgClubDto {
    _id?: string;

    @IsString()
    image?: string;

    @IsUUID()
    club: Clube;
}
