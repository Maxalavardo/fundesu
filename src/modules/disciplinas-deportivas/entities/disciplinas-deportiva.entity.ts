import { Clube } from 'src/modules/clubes/entities/clube.entity';
import { DisciplinasAtleta } from 'src/modules/disciplinas-atleta/entities/disciplinas-atleta.entity';
import { InstalacionDisciplina } from 'src/modules/instalacion-disciplina/entities/instalacion-disciplina.entity';
import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class DisciplinasDeportiva {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreDisciplina: string;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => DisciplinasAtleta, (disciplinadAtleta) => disciplinadAtleta.disciplina)
    disciplinadAtleta: DisciplinasAtleta;

    @OneToMany(() => Clube, (club) => club.disciplina)
    club: Clube;

    @OneToMany(() => InstalacionDisciplina, (instalacionDisciplina) => instalacionDisciplina.disciplina)
    instalacionDisciplina: InstalacionDisciplina;
}
