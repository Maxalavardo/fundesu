import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSuperficieInstalacionDto } from './dto/create-superficie-instalacion.dto';
import { UpdateSuperficieInstalacionDto } from './dto/update-superficie-instalacion.dto';
import { SuperficieInstalacion } from './entities/superficie-instalacion.entity';

@Injectable()
export class SuperficieInstalacionService {
    constructor(
        @InjectRepository(SuperficieInstalacion)
        private readonly superficieInstRepository: Repository<SuperficieInstalacion>,
    ) {}

    async create(request: CreateSuperficieInstalacionDto) {
        const superficie = await this.superficieInstRepository.create(request);
        return await this.superficieInstRepository.save(superficie);
    }

    async findAll() {
        return await this.superficieInstRepository.find();
    }

    async findOne(id: string) {
        const complemento = await this.superficieInstRepository
            .createQueryBuilder('i')
            .where('instalaciones_id = :id', { id });
        return await complemento.getMany();
    }

    async update(id: string, request: UpdateSuperficieInstalacionDto) {
        const upSuperficie = await this.superficieInstRepository.findOne({ where: { _id: id } });
        upSuperficie.instalaciones = request.instalaciones;
        await this.superficieInstRepository.merge(upSuperficie, request);
        return await this.superficieInstRepository.save(upSuperficie);
    }

    async remove(id: string) {
        return await this.superficieInstRepository.delete(id);
    }
}
