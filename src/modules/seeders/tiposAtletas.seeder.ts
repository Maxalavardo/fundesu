import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { TipoAtleta } from '../tipo-atleta/entities/tipo-atleta.entity';

@Injectable()
export class TipoAtletaSeeder implements Seeder {
    constructor(
        @InjectRepository(TipoAtleta)
        private readonly tipoAtletaRepository: Repository<TipoAtleta>,
    ) {}

    async seed(): Promise<any> {
        const { tiposAtletas } = JSON.parse(readFileSync('./src/modules/seeders/jsons/tiposAtletas.json', 'utf8'));

        try {
            for await (const tipoAtleta of tiposAtletas) {
                await this.tipoAtletaRepository.save({ nombreTipoAtleta: tipoAtleta });
            }
        } catch (exception: any) {
            console.log('Crear tipo atleta: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const tipoAtleta = await this.tipoAtletaRepository.find();
        return await this.tipoAtletaRepository.remove(tipoAtleta);
    }
}
