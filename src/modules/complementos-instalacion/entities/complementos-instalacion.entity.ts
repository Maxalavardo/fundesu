import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { MaterialComplemento } from 'src/modules/material-complemento/entities/material-complemento.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class ComplementosInstalacion {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreComplemento: string;

    @Column({ type: 'float' })
    medidaComplemento: number;

    @Column({ type: 'integer' })
    cantidadComplemento: number;

    @ManyToOne(() => InstalacionesDeportiva, (instalaciones) => instalaciones.complementos, { eager: true })
    instalaciones: InstalacionesDeportiva;

    @OneToMany(() => MaterialComplemento, (complementoMaterial) => complementoMaterial.complemento)
    complementoMaterial: MaterialComplemento;
}
