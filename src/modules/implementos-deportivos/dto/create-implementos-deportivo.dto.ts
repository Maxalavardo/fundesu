import { IsString } from 'class-validator';

export class CreateImplementosDeportivoDto {
    _id?: string;

    @IsString()
    nombreImplemento: string;
}
