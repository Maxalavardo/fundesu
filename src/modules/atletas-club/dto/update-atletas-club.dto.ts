import { PartialType } from '@nestjs/mapped-types';
import { CreateAtletasClubDto } from './create-atletas-club.dto';

export class UpdateAtletasClubDto extends PartialType(CreateAtletasClubDto) {}
