import { Module } from '@nestjs/common';
import { InstalacionDisciplinaService } from './instalacion-disciplina.service';
import { InstalacionDisciplinaController } from './instalacion-disciplina.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InstalacionDisciplina } from './entities/instalacion-disciplina.entity';

@Module({
    imports: [TypeOrmModule.forFeature([InstalacionDisciplina])],
    controllers: [InstalacionDisciplinaController],
    providers: [InstalacionDisciplinaService],
})
export class InstalacionDisciplinaModule {}
