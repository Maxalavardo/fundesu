import { IsInt, IsUUID } from 'class-validator';
import { ComplementosInstalacion } from 'src/modules/complementos-instalacion/entities/complementos-instalacion.entity';
import { Materiale } from 'src/modules/materiales/entities/materiale.entity';

export class CreateMaterialComplementoDto {
    _id?: string;

    @IsInt()
    cantidadMaterial: number;

    @IsUUID()
    material: Materiale;

    @IsUUID()
    complemento: ComplementosInstalacion;
}
