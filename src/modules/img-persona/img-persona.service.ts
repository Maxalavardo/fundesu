import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImgPersonaDto } from './dto/create-img-persona.dto';
import { UpdateImgPersonaDto } from './dto/update-img-persona.dto';
import { ImgPersona } from './entities/img-persona.entity';

@Injectable()
export class ImgPersonaService {
    constructor(
        @InjectRepository(ImgPersona)
        private readonly imgPersonaRepository: Repository<ImgPersona>,
    ) {}

    async create(request: CreateImgPersonaDto) {
        const img = await this.imgPersonaRepository.create(request);
        return await this.imgPersonaRepository.save(img);
    }

    async findAll() {
        return await this.imgPersonaRepository.find();
    }

    async findOne(id: string) {
        return await this.imgPersonaRepository.findOne({
            where: {
                persona: { _id: id },
            },
        });
    }

    async update(id: string, request: UpdateImgPersonaDto) {
        const upImg = await this.imgPersonaRepository.findOne({ where: { _id: id } });
        upImg.persona = request.persona;
        await this.imgPersonaRepository.merge(upImg, request);
        return await this.imgPersonaRepository.save(upImg);
    }

    async remove(id: string) {
        return await this.imgPersonaRepository.delete(id);
    }
}
