import { Module } from '@nestjs/common';
import { TipoInstalacionDepService } from './tipo-instalacion-dep.service';
import { TipoInstalacionDepController } from './tipo-instalacion-dep.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TipoInstalacionDep } from './entities/tipo-instalacion-dep.entity';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';

@Module({
    imports: [TypeOrmModule.forFeature([TipoInstalacionDep, Notification, Usuario])],
    controllers: [TipoInstalacionDepController],
    providers: [TipoInstalacionDepService],
})
export class TipoInstalacionDepModule {}
