import { Test, TestingModule } from '@nestjs/testing';
import { ComplejosDeportivosController } from './complejos-deportivos.controller';
import { ComplejosDeportivosService } from './complejos-deportivos.service';

describe('ComplejosDeportivosController', () => {
    let controller: ComplejosDeportivosController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ComplejosDeportivosController],
            providers: [ComplejosDeportivosService],
        }).compile();

        controller = module.get<ComplejosDeportivosController>(ComplejosDeportivosController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
