import { PartialType } from '@nestjs/mapped-types';
import { CreateEventosAtletaDto } from './create-eventos-atleta.dto';

export class UpdateEventosAtletaDto extends PartialType(CreateEventosAtletaDto) {}
