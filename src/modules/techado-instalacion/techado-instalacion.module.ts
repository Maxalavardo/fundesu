import { Module } from '@nestjs/common';
import { TechadoInstalacionService } from './techado-instalacion.service';
import { TechadoInstalacionController } from './techado-instalacion.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TechadoInstalacion } from './entities/techado-instalacion.entity';

@Module({
    imports: [TypeOrmModule.forFeature([TechadoInstalacion])],
    controllers: [TechadoInstalacionController],
    providers: [TechadoInstalacionService],
})
export class TechadoInstalacionModule {}
