import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ImgInstalacionesService } from './img-instalaciones.service';
import { CreateImgInstalacioneDto } from './dto/create-img-instalacione.dto';
import { UpdateImgInstalacioneDto } from './dto/update-img-instalacione.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('img-instalaciones')
@ApiTags('Img-instalaciones')
export class ImgInstalacionesController {
    constructor(private readonly imgInstalacionesService: ImgInstalacionesService) {}

    @Post()
    @ApiBody({ type: [CreateImgInstalacioneDto] })
    create(@Res() Res, @Body() createImgInstalacioneDto: CreateImgInstalacioneDto) {
        this.imgInstalacionesService.create(createImgInstalacioneDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Get()
    findAll() {
        return this.imgInstalacionesService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.imgInstalacionesService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImgInstalacioneDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateImgInstalacioneDto: UpdateImgInstalacioneDto) {
        return this.imgInstalacionesService.update(id, updateImgInstalacioneDto).then((img) => {
            return Res.status(HttpStatus.ACCEPTED).json(img);
        });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.imgInstalacionesService.remove(id);
    }
}
