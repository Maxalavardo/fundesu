import { Test, TestingModule } from '@nestjs/testing';
import { MaterialEquipamientoEspService } from './material-equipamiento-esp.service';

describe('MaterialEquipamientoEspService', () => {
    let service: MaterialEquipamientoEspService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MaterialEquipamientoEspService],
        }).compile();

        service = module.get<MaterialEquipamientoEspService>(MaterialEquipamientoEspService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
