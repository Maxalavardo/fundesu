import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { MaterialIluminacion } from 'src/modules/material-iluminacion/entities/material-iluminacion.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class IluminacionInstalacion {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreIluminacion: string;

    @Column({ type: 'integer' })
    cantidadIluminacion: number;

    @ManyToOne(() => InstalacionesDeportiva, (instalaciones) => instalaciones.iluminacion, { eager: true })
    instalaciones: InstalacionesDeportiva;

    @OneToMany(() => MaterialIluminacion, (iluminacionMaterial) => iluminacionMaterial.iluminacion)
    iluminacionMaterial: MaterialIluminacion;
}
