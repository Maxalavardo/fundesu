import { PartialType } from '@nestjs/mapped-types';
import { CreateImgParedDto } from './create-img-pared.dto';

export class UpdateImgParedDto extends PartialType(CreateImgParedDto) {}
