import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { CreateUsuarioDto } from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { Usuario } from './entities/usuario.entity';

@Injectable()
export class UsuariosService {
    constructor(
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
    ) {}

    async create(request: CreateUsuarioDto, user: any) {
        let usuario = this.usuarioRepository.create(request);

        const validateUser = await this.usuarioRepository
            .createQueryBuilder('i')
            .where('LOWER(i.usuario) = LOWER(:usuario)', { usuario: request.usuario })
            .getOne();

        if (validateUser) {
            return { msg: 'Usuario ya registrado' };
        }

        usuario = await this.usuarioRepository.save(usuario);

        /// =================================

        const message = 'Un usuario a sido registrado';

        const userValidate = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!userValidate) {
            throw new BadRequestException('Usuario logueado no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Usuario.name,
            uuid: usuario._id,
            data: usuario,
            user: userValidate,
        });

        await this.notificationRepository.save(notification);

        return usuario;
    }

    async findAll() {
        return await this.usuarioRepository.find();
    }

    async findOne(id: string) {
        return await this.usuarioRepository.findOne({ where: { _id: id } });
    }

    async findUser(usuario: string, contrasena: string) {
        const getUsuario = await this.usuarioRepository
            .createQueryBuilder('i')
            .select('i._id, i.usuario, i.contrasena, i.rol, i.club, i.municipio')
            .where('usuario = :usuario', { usuario })
            .andWhere('contrasena = :contrasena', { contrasena });
        return await getUsuario.getRawOne();
    }

    async update(id: string, request: UpdateUsuarioDto) {
        const upUsuario = await this.usuarioRepository.findOne({ where: { _id: id } });
        await this.usuarioRepository.merge(upUsuario, request);
        return await this.usuarioRepository.save(upUsuario);
    }

    async remove(id: string, user: any) {
        const usuario = await this.usuarioRepository.findOne({ where: { _id: id } });

        const message = 'Un usuario a sido eliminado';

        const userValidate = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!userValidate) {
            throw new BadRequestException('Usuario logueado no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Usuario.name,
            uuid: id,
            data: usuario,
            user: userValidate,
        });

        await this.notificationRepository.save(notification);

        return await this.usuarioRepository.softDelete(id);
    }

    // validaciones - funciones
    async emailExists(email: string, error = false) {
        const email_exists = await this.usuarioRepository.findOne({
            relations: { persona: true },
            where: { usuario: email },
        });

        if (email_exists && error) {
            throw new BadRequestException(`este usuario ya está registrado ${email_exists.usuario}`);
        }

        return email_exists;
    }
}
