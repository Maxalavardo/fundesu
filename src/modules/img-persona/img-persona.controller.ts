import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ImgPersonaService } from './img-persona.service';
import { CreateImgPersonaDto } from './dto/create-img-persona.dto';
import { UpdateImgPersonaDto } from './dto/update-img-persona.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('img-persona')
@ApiTags('Img-persona')
export class ImgPersonaController {
    constructor(private readonly imgPersonaService: ImgPersonaService) {}

    @Post()
    @ApiBody({ type: [CreateImgPersonaDto] })
    create(@Res() Res, @Body() createImgPersonaDto: CreateImgPersonaDto) {
        return this.imgPersonaService.create(createImgPersonaDto).then((img) => {
            return Res.status(HttpStatus.CREATED).json(img);
        });
    }

    @Get()
    findAll() {
        return this.imgPersonaService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.imgPersonaService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImgPersonaDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateImgPersonaDto: UpdateImgPersonaDto) {
        this.imgPersonaService.update(id, updateImgPersonaDto).then((img) => {
            return Res.status(HttpStatus.ACCEPTED).json(img);
        });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.imgPersonaService.remove(id);
    }
}
