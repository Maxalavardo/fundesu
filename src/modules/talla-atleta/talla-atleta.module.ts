import { Module } from '@nestjs/common';
import { TallaAtletaService } from './talla-atleta.service';
import { TallaAtletaController } from './talla-atleta.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TallaAtleta } from './entities/talla-atleta.entity';

@Module({
    imports: [TypeOrmModule.forFeature([TallaAtleta])],
    controllers: [TallaAtletaController],
    providers: [TallaAtletaService],
})
export class TallaAtletaModule {}
