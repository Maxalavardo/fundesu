import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { TipoInstalacionDep } from '../tipo-instalacion-dep/entities/tipo-instalacion-dep.entity';

@Injectable()
export class TypeInstSeeder implements Seeder {
    constructor(
        @InjectRepository(TipoInstalacionDep)
        private readonly tipoInstalacionDepRepository: Repository<TipoInstalacionDep>,
    ) {}

    async seed(): Promise<any> {
        const { instTypes } = JSON.parse(readFileSync('./src/modules/seeders/jsons/instTypes.json', 'utf8'));

        try {
            for await (const instType of instTypes) {
                await this.tipoInstalacionDepRepository.save({ nombreTipoInstalacion: instType });
            }
        } catch (exception: any) {
            console.log('Crear tipo instalacion deportiva: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const instTypes = await this.tipoInstalacionDepRepository.find();
        return await this.tipoInstalacionDepRepository.remove(instTypes);
    }
}
