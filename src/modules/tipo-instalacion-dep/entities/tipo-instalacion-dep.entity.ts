import { InstalacionDisciplina } from 'src/modules/instalacion-disciplina/entities/instalacion-disciplina.entity';
import { InstalacionesDeportiva } from 'src/modules/instalaciones-deportivas/entities/instalaciones-deportiva.entity';
import { Column, DeleteDateColumn, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class TipoInstalacionDep {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'varchar' })
    nombreTipoInstalacion: string;

    @DeleteDateColumn()
    deleted_at?: Date;

    @OneToMany(() => InstalacionesDeportiva, (instalacionDeportiva) => instalacionDeportiva.tipoInstalacionDep)
    instalacionDeportiva: InstalacionesDeportiva;

    @OneToMany(() => InstalacionDisciplina, (instalacionDisciplina) => instalacionDisciplina.tipoInstalacion)
    instalacionDisciplina: InstalacionDisciplina;
}
