import { PartialType } from '@nestjs/mapped-types';
import { CreateMaterialTechadoDto } from './create-material-techado.dto';

export class UpdateMaterialTechadoDto extends PartialType(CreateMaterialTechadoDto) {}
