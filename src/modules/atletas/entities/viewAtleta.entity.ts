import { ViewColumn, ViewEntity } from 'typeorm';

@ViewEntity({
    expression: `
        SELECT
        atleta._id,
        persona.cedula,
        persona.nombres,
        persona.apellidos,
        persona.sexo,
        persona.fechanac,
        persona.telefono,
        persona.tlfcasa,
        persona.direccion,
        atleta."tipoSangre",
        tipo_atleta."nombreTipoAtleta",
        disciplinas_deportiva."nombreDisciplina",
        disciplinas_atleta.categoria,
        disciplinas_atleta.division
        FROM 
        persona,
        atleta,
        tipo_atleta,
        disciplinas_deportiva,
        disciplinas_atleta
        WHERE
        persona._id=atleta.persona_id and
        tipo_atleta._id=atleta."tipoAtleta_id" and
        atleta._id=disciplinas_atleta.atleta_id and
        disciplinas_deportiva._id=disciplinas_atleta.disciplina_id;
    `,
})
export class ViewAtleta {
    @ViewColumn()
    _id: string;

    @ViewColumn()
    cedula: string;

    @ViewColumn()
    nombres: string;

    @ViewColumn()
    apellidos: string;

    @ViewColumn()
    sexo: string;

    @ViewColumn()
    fechanac: Date;

    @ViewColumn()
    telefono: string;

    @ViewColumn()
    tlfcasa: string;

    @ViewColumn()
    direccion: string;

    @ViewColumn()
    tipoSangre: string;

    @ViewColumn()
    nombreTipoAtleta: string;

    @ViewColumn()
    nombreDisciplina: string;

    @ViewColumn()
    categoria: string;

    @ViewColumn()
    division: string;
}
