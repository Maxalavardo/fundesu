import { Module } from '@nestjs/common';
import { CargosService } from './cargos.service';
import { CargosController } from './cargos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cargo } from './entities/cargo.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { Notification } from '../notification/entities/notification.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Cargo, Notification, Usuario])],
    controllers: [CargosController],
    providers: [CargosService],
})
export class CargosModule {}
