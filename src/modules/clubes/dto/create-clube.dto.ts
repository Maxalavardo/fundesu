import { IsDateString, IsString, IsUUID } from 'class-validator';
import { DisciplinasDeportiva } from 'src/modules/disciplinas-deportivas/entities/disciplinas-deportiva.entity';

export class CreateClubeDto {
    _id?: string;

    @IsString()
    nombreClub: string;

    @IsUUID()
    disciplina: DisciplinasDeportiva;

    @IsDateString()
    fechaCreacion: Date;
}
