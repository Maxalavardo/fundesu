import { Module } from '@nestjs/common';
import { MaterialParedService } from './material-pared.service';
import { MaterialParedController } from './material-pared.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MaterialPared } from './entities/material-pared.entity';
import { Materiale } from '../materiales/entities/materiale.entity';

@Module({
    imports: [TypeOrmModule.forFeature([MaterialPared, Materiale])],
    controllers: [MaterialParedController],
    providers: [MaterialParedService],
})
export class MaterialParedModule {}
