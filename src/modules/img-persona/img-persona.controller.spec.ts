import { Test, TestingModule } from '@nestjs/testing';
import { ImgPersonaController } from './img-persona.controller';
import { ImgPersonaService } from './img-persona.service';

describe('ImgPersonaController', () => {
    let controller: ImgPersonaController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ImgPersonaController],
            providers: [ImgPersonaService],
        }).compile();

        controller = module.get<ImgPersonaController>(ImgPersonaController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
