import { Controller, Get, Post, Body, Patch, Param, Delete, HttpStatus, Res } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { ComplementosInstalacionService } from './complementos-instalacion.service';
import { CreateComplementosInstalacionDto } from './dto/create-complementos-instalacion.dto';
import { UpdateComplementosInstalacionDto } from './dto/update-complementos-instalacion.dto';

@Controller('complementos-instalacion')
@ApiTags('Complementos-instalacion')
export class ComplementosInstalacionController {
    constructor(private readonly complementosInstalacionService: ComplementosInstalacionService) {}

    @Post()
    @ApiBody({ type: [CreateComplementosInstalacionDto] })
    create(@Res() Res, @Body() createComplementosInstalacionDto: CreateComplementosInstalacionDto) {
        this.complementosInstalacionService
            .create(createComplementosInstalacionDto)
            .then((complemento) => {
                return Res.status(HttpStatus.CREATED).json(complemento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.complementosInstalacionService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.complementosInstalacionService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateComplementosInstalacionDto] })
    update(
        @Res() Res,
        @Param('id') id: string,
        @Body() updateComplementosInstalacionDto: UpdateComplementosInstalacionDto,
    ) {
        this.complementosInstalacionService
            .update(id, updateComplementosInstalacionDto)
            .then((complemento) => {
                return Res.status(HttpStatus.CREATED).json(complemento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.complementosInstalacionService.remove(id);
    }
}
