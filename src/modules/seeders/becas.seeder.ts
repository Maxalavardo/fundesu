import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { readFileSync } from 'fs';
import { Seeder } from 'nestjs-seeder';
import { Repository } from 'typeorm';
import { Beca } from '../becas/entities/beca.entity';

@Injectable()
export class BecasSeeder implements Seeder {
    constructor(
        @InjectRepository(Beca)
        private readonly becasRepository: Repository<Beca>,
    ) {}

    async seed(): Promise<any> {
        const { becas } = JSON.parse(readFileSync('./src/modules/seeders/jsons/becas.json', 'utf8'));

        try {
            for await (const beca of becas) {
                await this.becasRepository.save({ nombreBeca: beca.nombre, monto: beca.monto });
            }
        } catch (exception: any) {
            console.log('Crear tipo instalacion deportiva: ', exception.message);
        }
        return true;
    }

    async drop(): Promise<any> {
        const beca = await this.becasRepository.find();
        return await this.becasRepository.remove(beca);
    }
}
