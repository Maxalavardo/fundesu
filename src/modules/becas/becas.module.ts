import { Module } from '@nestjs/common';
import { BecasService } from './becas.service';
import { BecasController } from './becas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Beca } from './entities/beca.entity';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Beca, Notification, Usuario])],
    controllers: [BecasController],
    providers: [BecasService],
})
export class BecasModule {}
