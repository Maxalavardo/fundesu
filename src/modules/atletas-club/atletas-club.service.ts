import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAtletasClubDto } from './dto/create-atletas-club.dto';
import { UpdateAtletasClubDto } from './dto/update-atletas-club.dto';
import { AtletasClub } from './entities/atletas-club.entity';

@Injectable()
export class AtletasClubService {
    constructor(
        @InjectRepository(AtletasClub)
        private readonly atletaClubRepository: Repository<AtletasClub>,
    ) {}

    async create(request: CreateAtletasClubDto) {
        const atletaClub = await this.atletaClubRepository.create(request);
        return await this.atletaClubRepository.save(atletaClub);
    }

    async findAll() {
        return await this.atletaClubRepository.find();
    }

    async findOne(id: string) {
        return await this.atletaClubRepository.findOne({
            where: {
                atleta: {
                    _id: id,
                },
            },
        });
    }

    async update(id: string, request: UpdateAtletasClubDto) {
        const upAtletaClub = await this.atletaClubRepository.findOne({ where: { _id: id } });
        upAtletaClub.atleta = request.atleta;
        upAtletaClub.club = request.club;
        await this.atletaClubRepository.merge(upAtletaClub, request);
        return await this.atletaClubRepository.save(upAtletaClub);
    }

    async remove(id: string) {
        return await this.atletaClubRepository.delete(id);
    }
}
