import { Test, TestingModule } from '@nestjs/testing';
import { EquipamientoDepotivoEspService } from './equipamiento-depotivo-esp.service';

describe('EquipamientoDepotivoEspService', () => {
    let service: EquipamientoDepotivoEspService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [EquipamientoDepotivoEspService],
        }).compile();

        service = module.get<EquipamientoDepotivoEspService>(EquipamientoDepotivoEspService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
