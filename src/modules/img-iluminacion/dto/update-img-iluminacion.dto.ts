import { PartialType } from '@nestjs/mapped-types';
import { CreateImgIluminacionDto } from './create-img-iluminacion.dto';

export class UpdateImgIluminacionDto extends PartialType(CreateImgIluminacionDto) {}
