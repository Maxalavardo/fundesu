import { PartialType } from '@nestjs/mapped-types';
import { CreateEquipamientoInstalacionDto } from './create-equipamiento-instalacion.dto';

export class UpdateEquipamientoInstalacionDto extends PartialType(CreateEquipamientoInstalacionDto) {}
