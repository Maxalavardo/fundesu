import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { MaterialTechadoService } from './material-techado.service';
import { CreateMaterialTechadoDto } from './dto/create-material-techado.dto';
import { UpdateMaterialTechadoDto } from './dto/update-material-techado.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('material-techado')
@ApiTags('Material-techado')
export class MaterialTechadoController {
    constructor(private readonly materialTechadoService: MaterialTechadoService) {}

    @Post()
    @ApiBody({ type: [CreateMaterialTechadoDto] })
    create(@Res() Res, @Body() createMaterialTechadoDto: CreateMaterialTechadoDto) {
        this.materialTechadoService
            .create(createMaterialTechadoDto)
            .then((techado) => {
                return Res.status(HttpStatus.CREATED).json(techado);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.materialTechadoService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.materialTechadoService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateMaterialTechadoDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateMaterialTechadoDto: UpdateMaterialTechadoDto) {
        this.materialTechadoService
            .update(id, updateMaterialTechadoDto)
            .then((techado) => {
                return Res.status(HttpStatus.ACCEPTED).json(techado);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.materialTechadoService.remove(id);
    }
}
