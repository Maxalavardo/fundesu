import { Module } from '@nestjs/common';
import { DiscapacidadesAtletaService } from './discapacidades-atleta.service';
import { DiscapacidadesAtletaController } from './discapacidades-atleta.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DiscapacidadesAtleta } from './entities/discapacidades-atleta.entity';
import { Discapacidade } from '../discapacidades/entities/discapacidade.entity';

@Module({
    imports: [TypeOrmModule.forFeature([DiscapacidadesAtleta, Discapacidade])],
    controllers: [DiscapacidadesAtletaController],
    providers: [DiscapacidadesAtletaService],
})
export class DiscapacidadesAtletaModule {}
