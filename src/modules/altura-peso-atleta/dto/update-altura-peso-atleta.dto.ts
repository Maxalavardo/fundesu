import { PartialType } from '@nestjs/mapped-types';
import { CreateAlturaPesoAtletaDto } from './create-altura-peso-atleta.dto';

export class UpdateAlturaPesoAtletaDto extends PartialType(CreateAlturaPesoAtletaDto) {}
