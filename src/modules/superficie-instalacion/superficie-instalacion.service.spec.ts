import { Test, TestingModule } from '@nestjs/testing';
import { SuperficieInstalacionService } from './superficie-instalacion.service';

describe('SuperficieInstalacionService', () => {
    let service: SuperficieInstalacionService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [SuperficieInstalacionService],
        }).compile();

        service = module.get<SuperficieInstalacionService>(SuperficieInstalacionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
