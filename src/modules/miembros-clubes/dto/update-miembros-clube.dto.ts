import { PartialType } from '@nestjs/mapped-types';
import { CreateMiembrosClubeDto } from './create-miembros-clube.dto';

export class UpdateMiembrosClubeDto extends PartialType(CreateMiembrosClubeDto) {}
