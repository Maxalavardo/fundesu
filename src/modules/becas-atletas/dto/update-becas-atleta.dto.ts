import { PartialType } from '@nestjs/mapped-types';
import { CreateBecasAtletaDto } from './create-becas-atleta.dto';

export class UpdateBecasAtletaDto extends PartialType(CreateBecasAtletaDto) {}
