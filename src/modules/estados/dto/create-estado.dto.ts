import { IsNotEmpty, IsString } from 'class-validator';

export class CreateEstadoDto {
    _id?: string;

    @IsNotEmpty()
    @IsString()
    nombreEstado: string;
}
