import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateClubeDto } from './dto/create-clube.dto';
import { UpdateClubeDto } from './dto/update-clube.dto';
import { Clube } from './entities/clube.entity';

@Injectable()
export class ClubesService {
    constructor(
        @InjectRepository(Clube)
        private readonly clubRepository: Repository<Clube>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    async create(request: CreateClubeDto, user: any) {
        let club = this.clubRepository.create(request);
        club = await this.clubRepository.save(club);

        const message = 'Un club a sido registrado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Clube.name,
            uuid: club._id,
            data: club,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return club;
    }

    async findAll() {
        return await this.clubRepository.find();
    }

    async findOne(id: string) {
        return await this.clubRepository.findOne({ where: { _id: id } });
    }

    async count() {
        const count = await this.clubRepository.count();
        return count;
    }

    async update(id: string, request: UpdateClubeDto) {
        const upClub = await this.clubRepository.findOne({ where: { _id: id } });
        upClub.disciplina = request.disciplina;
        this.clubRepository.merge(upClub, request);
        return await this.clubRepository.save(upClub);
    }

    async remove(id: string, user: any) {
        const club = await this.clubRepository.findOne({ where: { _id: id } });

        const message = 'Un club a sido eliminado';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Clube.name,
            uuid: id,
            data: club,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.clubRepository.softDelete(id);
    }
}
