import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { DiscapacidadesService } from './discapacidades.service';
import { CreateDiscapacidadeDto } from './dto/create-discapacidade.dto';
import { UpdateDiscapacidadeDto } from './dto/update-discapacidade.dto';

@Controller('discapacidades')
@ApiTags('Discapacidades')
export class DiscapacidadesController {
    constructor(private readonly discapacidadesService: DiscapacidadesService) {}

    @Post()
    @ApiBody({ type: [CreateDiscapacidadeDto] })
    create(@Res() Res, @Body() createDiscapacidadeDto: CreateDiscapacidadeDto) {
        this.discapacidadesService
            .create(createDiscapacidadeDto)
            .then((discapacidad) => {
                return Res.status(HttpStatus.CREATED).json(discapacidad);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.discapacidadesService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.discapacidadesService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateDiscapacidadeDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateDiscapacidadeDto: UpdateDiscapacidadeDto) {
        this.discapacidadesService
            .update(id, updateDiscapacidadeDto)
            .then((discapacidad) => {
                return Res.status(HttpStatus.ACCEPTED).json(discapacidad);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.discapacidadesService.remove(id);
    }
}
