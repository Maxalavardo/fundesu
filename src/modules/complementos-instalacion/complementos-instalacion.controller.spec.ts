import { Test, TestingModule } from '@nestjs/testing';
import { ComplementosInstalacionController } from './complementos-instalacion.controller';
import { ComplementosInstalacionService } from './complementos-instalacion.service';

describe('ComplementosInstalacionController', () => {
    let controller: ComplementosInstalacionController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ComplementosInstalacionController],
            providers: [ComplementosInstalacionService],
        }).compile();

        controller = module.get<ComplementosInstalacionController>(ComplementosInstalacionController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
