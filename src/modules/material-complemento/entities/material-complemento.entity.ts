import { Materiale } from '../../materiales/entities/materiale.entity';
import { ComplementosInstalacion } from '../../complementos-instalacion/entities/complementos-instalacion.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';

@Entity()
export class MaterialComplemento {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'integer' })
    cantidadMaterial: number;

    @ManyToOne(() => Materiale, (material) => material.complementoMaterial, { eager: true })
    material: Materiale;

    @ManyToOne(() => ComplementosInstalacion, (complemento) => complemento.complementoMaterial, { eager: true })
    complemento: ComplementosInstalacion;
}
