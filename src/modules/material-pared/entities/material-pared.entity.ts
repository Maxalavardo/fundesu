import { Materiale } from '../../materiales/entities/materiale.entity';
import { ParedInstalacion } from '../../pared-instalacion/entities/pared-instalacion.entity';
import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';

@Entity()
export class MaterialPared {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @Column({ type: 'integer' })
    cantidadMaterial: number;

    @ManyToOne(() => Materiale, (material) => material.paredMaterial, { eager: true })
    material: Materiale;

    @ManyToOne(() => ParedInstalacion, (pared) => pared.paredMaterial, { eager: true })
    pared: ParedInstalacion;
}
