import { Test, TestingModule } from '@nestjs/testing';
import { EquipamientoInstalacionController } from './equipamiento-instalacion.controller';
import { EquipamientoInstalacionService } from './equipamiento-instalacion.service';

describe('EquipamientoInstalacionController', () => {
    let controller: EquipamientoInstalacionController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [EquipamientoInstalacionController],
            providers: [EquipamientoInstalacionService],
        }).compile();

        controller = module.get<EquipamientoInstalacionController>(EquipamientoInstalacionController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
