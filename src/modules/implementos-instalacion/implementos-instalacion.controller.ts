import { Controller, Get, Post, Body, Patch, Param, Delete, Res, HttpStatus } from '@nestjs/common';
import { ImplementosInstalacionService } from './implementos-instalacion.service';
import { CreateImplementosInstalacionDto } from './dto/create-implementos-instalacion.dto';
import { UpdateImplementosInstalacionDto } from './dto/update-implementos-instalacion.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('implementos-instalacion')
@ApiTags('Implementos-instalacion')
export class ImplementosInstalacionController {
    constructor(private readonly implementosInstalacionService: ImplementosInstalacionService) {}

    @Post()
    @ApiBody({ type: [CreateImplementosInstalacionDto] })
    create(@Res() Res, @Body() createImplementosInstalacionDto: CreateImplementosInstalacionDto) {
        this.implementosInstalacionService
            .create(createImplementosInstalacionDto)
            .then((implemento) => {
                return Res.status(HttpStatus.CREATED).json(implemento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    findAll() {
        return this.implementosInstalacionService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
        return this.implementosInstalacionService.findOne(id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateImplementosInstalacionDto] })
    update(
        @Res() Res,
        @Param('id') id: string,
        @Body() updateImplementosInstalacionDto: UpdateImplementosInstalacionDto,
    ) {
        this.implementosInstalacionService
            .update(id, updateImplementosInstalacionDto)
            .then((implemento) => {
                return Res.status(HttpStatus.ACCEPTED).json(implemento);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.implementosInstalacionService.remove(id);
    }
}
