import { Test, TestingModule } from '@nestjs/testing';
import { MaterialIluminacionService } from './material-iluminacion.service';

describe('MaterialIluminacionService', () => {
    let service: MaterialIluminacionService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MaterialIluminacionService],
        }).compile();

        service = module.get<MaterialIluminacionService>(MaterialIluminacionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
