import { PartialType } from '@nestjs/mapped-types';
import { CreateComplejosDeportivoDto } from './create-complejos-deportivo.dto';

export class UpdateComplejosDeportivoDto extends PartialType(CreateComplejosDeportivoDto) {}
