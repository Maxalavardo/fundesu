import { Test, TestingModule } from '@nestjs/testing';
import { BecasAtletasController } from './becas-atletas.controller';
import { BecasAtletasService } from './becas-atletas.service';

describe('BecasAtletasController', () => {
    let controller: BecasAtletasController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [BecasAtletasController],
            providers: [BecasAtletasService],
        }).compile();

        controller = module.get<BecasAtletasController>(BecasAtletasController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
