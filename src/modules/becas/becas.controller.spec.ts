import { Test, TestingModule } from '@nestjs/testing';
import { BecasController } from './becas.controller';
import { BecasService } from './becas.service';

describe('BecasController', () => {
    let controller: BecasController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [BecasController],
            providers: [BecasService],
        }).compile();

        controller = module.get<BecasController>(BecasController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
