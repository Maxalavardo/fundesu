import { Test, TestingModule } from '@nestjs/testing';
import { BecasAtletasService } from './becas-atletas.service';

describe('BecasAtletasService', () => {
    let service: BecasAtletasService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [BecasAtletasService],
        }).compile();

        service = module.get<BecasAtletasService>(BecasAtletasService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
