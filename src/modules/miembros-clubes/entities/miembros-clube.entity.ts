import { Cargo } from 'src/modules/cargos/entities/cargo.entity';
import { Clube } from 'src/modules/clubes/entities/clube.entity';
import { Persona } from 'src/modules/personas/entities/persona.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class MiembrosClube {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @ManyToOne(() => Persona, (persona) => persona.miembroClub, { eager: true })
    persona: Persona;

    @ManyToOne(() => Clube, (club) => club.miembroClub, { eager: true })
    club: Clube;

    @ManyToOne(() => Cargo, (cargo) => cargo.miembroClub, { eager: true })
    cargo: Cargo;

    @Column({ type: 'date' })
    fechaIngreso: Date;

    @Column({ type: 'date', nullable: true })
    fechaSalida: Date;

    @Column({ type: 'boolean' })
    estatus: boolean;
}
