import { IsNotEmpty, IsString, IsUUID } from 'class-validator';
import { Estado } from 'src/modules/estados/entities/estado.entity';

export class CreateMunicipioDto {
    _id?: string;

    @IsNotEmpty()
    @IsString()
    nombreMunicipio: string;

    @IsNotEmpty()
    @IsUUID()
    estado: Estado;
}
