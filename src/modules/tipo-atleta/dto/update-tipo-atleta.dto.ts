import { PartialType } from '@nestjs/mapped-types';
import { CreateTipoAtletaDto } from './create-tipo-atleta.dto';

export class UpdateTipoAtletaDto extends PartialType(CreateTipoAtletaDto) {}
