import { Controller, Get, Post, Body, Param, HttpStatus, Res, Patch, Delete, Query } from '@nestjs/common';
import { SectoresService } from './sectores.service';
import { CreateSectoreDto } from './dto/create-sectore.dto';
import { UpdateSectoreDto } from './dto/update-sectore.dto';
import { ApiBody, ApiTags } from '@nestjs/swagger';

@Controller('sectores')
@ApiTags('Sectores')
export class SectoresController {
    constructor(private readonly sectoresService: SectoresService) {}

    @Post()
    @ApiBody({ type: [CreateSectoreDto] })
    create(@Res() Res, @Body() createSectoreDto: CreateSectoreDto) {
        this.sectoresService
            .create(createSectoreDto)
            .then((sector) => {
                return Res.status(HttpStatus.CREATED).json(sector);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Get()
    async findAll(@Query('comunidadId') comunidadId: string) {
        return await this.sectoresService.findAll(comunidadId);
    }

    @Get(':id')
    async findOne(@Param('id') _id: string) {
        return await this.sectoresService.findOne(_id);
    }

    @Patch(':id')
    @ApiBody({ type: [CreateSectoreDto] })
    update(@Res() Res, @Param('id') id: string, @Body() updateSectorDto: UpdateSectoreDto) {
        this.sectoresService
            .update(id, updateSectorDto)
            .then((sector) => {
                return Res.status(HttpStatus.ACCEPTED).json(sector);
            })
            .catch((e) => {
                return Res.status(HttpStatus.NOT_FOUND).json(e);
            });
    }

    @Delete(':id')
    remove(@Param('id') id: string) {
        return this.sectoresService.remove(id);
    }
}
