import { Test, TestingModule } from '@nestjs/testing';
import { DisciplinasAtletaService } from './disciplinas-atleta.service';

describe('DisciplinasAtletaService', () => {
    let service: DisciplinasAtletaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [DisciplinasAtletaService],
        }).compile();

        service = module.get<DisciplinasAtletaService>(DisciplinasAtletaService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
