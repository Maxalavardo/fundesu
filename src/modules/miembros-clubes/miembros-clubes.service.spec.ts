import { Test, TestingModule } from '@nestjs/testing';
import { MiembrosClubesService } from './miembros-clubes.service';

describe('MiembrosClubesService', () => {
    let service: MiembrosClubesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MiembrosClubesService],
        }).compile();

        service = module.get<MiembrosClubesService>(MiembrosClubesService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
