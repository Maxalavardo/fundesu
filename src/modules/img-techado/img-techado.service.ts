import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateImgTechadoDto } from './dto/create-img-techado.dto';
import { UpdateImgTechadoDto } from './dto/update-img-techado.dto';
import { ImgTechado } from './entities/img-techado.entity';

@Injectable()
export class ImgTechadoService {
    constructor(
        @InjectRepository(ImgTechado)
        private readonly imgTechadoRepository: Repository<ImgTechado>,
    ) {}

    async create(request: CreateImgTechadoDto) {
        const img = await this.imgTechadoRepository.create(request);
        return await this.imgTechadoRepository.save(img);
    }

    async findAll() {
        return await this.imgTechadoRepository.find();
    }

    async findOne(id: string) {
        return await this.imgTechadoRepository.findOne({
            where: {
                instalaciones: { _id: id },
            },
        });
    }

    async update(id: string, request: UpdateImgTechadoDto) {
        const upImg = await this.imgTechadoRepository.findOne({ where: { _id: id } });
        upImg.instalaciones = request.instalaciones;
        await this.imgTechadoRepository.merge(upImg, request);
        return await this.imgTechadoRepository.save(upImg);
    }

    async remove(id: string) {
        return await this.imgTechadoRepository.delete(id);
    }
}
