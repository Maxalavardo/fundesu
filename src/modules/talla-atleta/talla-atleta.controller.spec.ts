import { Test, TestingModule } from '@nestjs/testing';
import { TallaAtletaController } from './talla-atleta.controller';
import { TallaAtletaService } from './talla-atleta.service';

describe('TallaAtletaController', () => {
    let controller: TallaAtletaController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [TallaAtletaController],
            providers: [TallaAtletaService],
        }).compile();

        controller = module.get<TallaAtletaController>(TallaAtletaController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
