import { IsUUID } from 'class-validator';
import { Clube } from 'src/modules/clubes/entities/clube.entity';
import { ComplejosDeportivo } from 'src/modules/complejos-deportivos/entities/complejos-deportivo.entity';

export class CreateComplejosClubeDto {
    _id: string;

    @IsUUID()
    complejo: ComplejosDeportivo;

    @IsUUID()
    club: Clube;
}
