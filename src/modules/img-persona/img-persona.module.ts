import { Module } from '@nestjs/common';
import { ImgPersonaService } from './img-persona.service';
import { ImgPersonaController } from './img-persona.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ImgPersona } from './entities/img-persona.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ImgPersona])],
    controllers: [ImgPersonaController],
    providers: [ImgPersonaService],
})
export class ImgPersonaModule {}
