import { Test, TestingModule } from '@nestjs/testing';
import { TipoInstalacionDepService } from './tipo-instalacion-dep.service';

describe('TipoInstalacionDepService', () => {
    let service: TipoInstalacionDepService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [TipoInstalacionDepService],
        }).compile();

        service = module.get<TipoInstalacionDepService>(TipoInstalacionDepService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
