import { Test, TestingModule } from '@nestjs/testing';
import { MaterialParedService } from './material-pared.service';

describe('MaterialParedService', () => {
    let service: MaterialParedService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MaterialParedService],
        }).compile();

        service = module.get<MaterialParedService>(MaterialParedService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
