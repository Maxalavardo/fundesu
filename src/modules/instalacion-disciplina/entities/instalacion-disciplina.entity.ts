import { DisciplinasDeportiva } from 'src/modules/disciplinas-deportivas/entities/disciplinas-deportiva.entity';
import { TipoInstalacionDep } from 'src/modules/tipo-instalacion-dep/entities/tipo-instalacion-dep.entity';
import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class InstalacionDisciplina {
    @PrimaryGeneratedColumn('uuid')
    _id: string;

    @ManyToOne(() => TipoInstalacionDep, (tipoInstalacion) => tipoInstalacion.instalacionDisciplina, { eager: true })
    tipoInstalacion: TipoInstalacionDep;

    @ManyToOne(() => DisciplinasDeportiva, (disciplina) => disciplina.instalacionDisciplina, { eager: true })
    disciplina: DisciplinasDeportiva;
}
