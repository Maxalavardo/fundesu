import { Test, TestingModule } from '@nestjs/testing';
import { AlturaPesoAtletaService } from './altura-peso-atleta.service';

describe('AlturaPesoAtletaService', () => {
    let service: AlturaPesoAtletaService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [AlturaPesoAtletaService],
        }).compile();

        service = module.get<AlturaPesoAtletaService>(AlturaPesoAtletaService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
