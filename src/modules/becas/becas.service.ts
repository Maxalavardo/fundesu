import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../notification/entities/notification.entity';
import { Usuario } from '../usuarios/entities/usuario.entity';
import { CreateBecaDto } from './dto/create-beca.dto';
import { UpdateBecaDto } from './dto/update-beca.dto';
import { Beca } from './entities/beca.entity';

@Injectable()
export class BecasService {
    constructor(
        @InjectRepository(Beca)
        private readonly becaRepository: Repository<Beca>,
        @InjectRepository(Notification)
        private readonly notificationRepository: Repository<Notification>,
        @InjectRepository(Usuario)
        private readonly usuarioRepository: Repository<Usuario>,
    ) {}

    async create(request: CreateBecaDto, user: any) {
        let beca = this.becaRepository.create(request);
        beca = await this.becaRepository.save(beca);

        const message = 'Una beca a sido registrada';

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Beca.name,
            uuid: beca._id,
            data: beca,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return beca;
    }

    async findAll() {
        return await this.becaRepository.find();
    }

    async findOne(id: string) {
        return await this.becaRepository.findOne({ where: { _id: id } });
    }

    async update(id: string, request: UpdateBecaDto) {
        const upBeca = await this.becaRepository.findOne({ where: { _id: id } });
        this.becaRepository.merge(upBeca, request);
        return await this.becaRepository.save(upBeca);
    }

    async remove(id: string, user: any) {
        const beca = await this.becaRepository.findOne({ where: { _id: id } });

        const usuario = await this.usuarioRepository.findOne({ where: { _id: user._id } });

        if (!usuario) {
            throw new BadRequestException('Usuario no encontrado');
        }

        const message = 'Una beca a sido eliminada';

        const notification = this.notificationRepository.create({
            notification: message,
            entity: Beca.name,
            uuid: id,
            data: beca,
            user: usuario,
        });

        await this.notificationRepository.save(notification);

        return await this.becaRepository.softDelete(id);
    }
}
